package szczepanski.gerard.betty.integration.service.user;

import szczepanski.gerard.betty.service.values.DomainId;

public interface CurrentBettyUserService {
	
	DomainId getCurrentUserId();
	
}
