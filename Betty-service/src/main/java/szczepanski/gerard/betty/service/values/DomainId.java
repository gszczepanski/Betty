package szczepanski.gerard.betty.service.values;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.betty.common.assertion.ParamAssertion;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class DomainId {
	
	private final Long id;
	
	public static final DomainId of(Long id) {
		ParamAssertion.isValidLongId(id);
		return new DomainId(id);
	}
	
	public Long get() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DomainId other = (DomainId) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return id.toString();
	}
	
}
