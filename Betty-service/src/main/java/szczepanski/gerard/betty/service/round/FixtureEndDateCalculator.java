package szczepanski.gerard.betty.service.round;

import java.time.LocalDateTime;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
class FixtureEndDateCalculator {
	
	protected static final Long HOURS_TO_FIXTURE_END_ADDED = 2L;
	
	public static LocalDateTime getFixtureEndDate(LocalDateTime startDate) {
		return startDate.plusHours(HOURS_TO_FIXTURE_END_ADDED);
	}
	
}
