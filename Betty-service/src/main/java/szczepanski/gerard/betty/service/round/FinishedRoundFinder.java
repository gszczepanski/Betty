package szczepanski.gerard.betty.service.round;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.betty.common.logger.BettyLogger;
import szczepanski.gerard.betty.common.logger.BettyLoggerFactory;
import szczepanski.gerard.betty.domain.league.round.RoundQueryRepository;

@Builder
@RequiredArgsConstructor
public class FinishedRoundFinder {
	private static final BettyLogger LOG = BettyLoggerFactory.createForName(FinishedRoundFinder.class.getName());
	
	private final RoundQueryRepository roundQueryRepository;
	
	public void findThenPublishFinishedRounds() {
		LOG.debug("Starting to find just finished rounds");
	}
	
	
}
