package szczepanski.gerard.betty.service.bet;

import java.util.Set;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import szczepanski.gerard.betty.service.values.DomainId;

@Setter
@Getter
@Builder
public class BetRoundDto {

	private DomainId roundId;
	private Set<BetFixtureDto> betFixtures;

	@Override
	public String toString() {
		return "BetRoundDto [roundId=" + roundId + ", betFixtures=" + betFixtures + "]";
	}

	@Setter
	@Getter
	@Builder
	public static class BetFixtureDto {

		private DomainId roundFixtureId;
		private Integer homeGoals;
		private Integer awayGoals;

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((roundFixtureId == null) ? 0 : roundFixtureId.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			BetFixtureDto other = (BetFixtureDto) obj;
			if (roundFixtureId == null) {
				if (other.roundFixtureId != null)
					return false;
			} else if (!roundFixtureId.equals(other.roundFixtureId))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "BetFixtureDto [roundFixtureId=" + roundFixtureId + ", homeGoals=" + homeGoals + ", awayGoals=" + awayGoals + "]";
		}

	}

}
