package szczepanski.gerard.betty.service.round;

import szczepanski.gerard.betty.service.common.validator.ValidatorException;
import szczepanski.gerard.betty.service.values.Code;

public interface RoundManagmentService {
	
	Code createRound(RoundCreationDto roundCreationDto) throws ValidatorException; 
	
}
