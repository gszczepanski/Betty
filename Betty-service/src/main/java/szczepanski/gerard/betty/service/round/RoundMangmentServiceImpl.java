package szczepanski.gerard.betty.service.round;

import lombok.AllArgsConstructor;
import lombok.Builder;
import szczepanski.gerard.betty.common.assertion.ParamAssertion;
import szczepanski.gerard.betty.common.logger.BettyLogger;
import szczepanski.gerard.betty.common.logger.BettyLoggerFactory;
import szczepanski.gerard.betty.domain.league.round.RoundCommandRepository;
import szczepanski.gerard.betty.domain.league.round.model.Round;
import szczepanski.gerard.betty.service.common.lock.AutoReleaseLock;
import szczepanski.gerard.betty.service.common.validator.ValidatorException;
import szczepanski.gerard.betty.service.values.Code;

@AllArgsConstructor
@Builder
class RoundMangmentServiceImpl implements RoundManagmentService {
	private static final BettyLogger LOG = BettyLoggerFactory.createForName(RoundManagmentService.class.getName());

	private final RoundCreationValidator roundCreationValidator;
	private final RoundCommandRepository roundCommandRepository;
	private final RoundFactory roundFactory;

	@Override
	public Code createRound(RoundCreationDto roundCreationDto) throws ValidatorException {
		ParamAssertion.isNotNull(roundCreationDto);
		roundCreationValidator.validate(roundCreationDto);
		LOG.debug("Create new round with dto: %s ", roundCreationDto);

		try (AutoReleaseLock lock = AutoReleaseLock.fromReentrantLock()) {
			lock.lock();
			return createRoundFromDto(roundCreationDto);
		}
	}
	
	private Code createRoundFromDto(RoundCreationDto roundCreationDto) throws ValidatorException {
		Round newRound = roundFactory.createRound(roundCreationDto);
		roundCommandRepository.create(newRound);
		return Code.of(newRound.getCode());
	}

}
