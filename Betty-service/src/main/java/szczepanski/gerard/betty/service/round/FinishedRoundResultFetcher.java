package szczepanski.gerard.betty.service.round;

import java.util.HashSet;
import java.util.Set;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.betty.common.logger.BettyLogger;
import szczepanski.gerard.betty.common.logger.BettyLoggerFactory;
import szczepanski.gerard.betty.integration.service.fixture.FixtureIntegrationService;
import szczepanski.gerard.betty.service.common.eventbus.EventBusProvider;
import szczepanski.gerard.betty.service.values.Code;

@RequiredArgsConstructor
public class FinishedRoundResultFetcher {
	private static final BettyLogger LOG = BettyLoggerFactory.createForName(FinishedRoundResultFetcher.class.getName());

	private final Set<Code> finishedRoundsCodes;
	private final FixtureIntegrationService fixtureIntegrationService;

	public FinishedRoundResultFetcher(FixtureIntegrationService fixtureIntegrationService) {
		super();
		this.finishedRoundsCodes = new HashSet<>();
		this.fixtureIntegrationService = fixtureIntegrationService;

		EventBusProvider.get().register(this);
	}

	public void fetchResultsForRound() {
		LOG.debug("Fetching round results for finished rounds with codes: %s", finishedRoundsCodes);
	}

}
