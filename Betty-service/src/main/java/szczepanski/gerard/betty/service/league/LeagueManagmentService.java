package szczepanski.gerard.betty.service.league;

public interface LeagueManagmentService {
	
	Long createLeague(LeagueCreationDto creationDto); 
	
}
