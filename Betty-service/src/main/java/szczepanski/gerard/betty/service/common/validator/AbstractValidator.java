package szczepanski.gerard.betty.service.common.validator;

public abstract class AbstractValidator<T> {
	private NotificationContainer notificationContainer;
	private FieldValidationHandler fieldValidator;

	/**
	 * Service validate method.
	 *
	 * @param obj
	 *            - object to validate.
	 * @throws ValidatorException
	 *             - is being thrown when validation fails. It has
	 *             NotificationContainer object with validation Notifications.
	 */
	public void validate(T obj) throws ValidatorException {
		setUp();
		doValidateObject(obj);
		checkNotifications();
	}

	protected void setUp() {
		notificationContainer = new NotificationContainer();
		fieldValidator = new FieldValidationHandler(notificationContainer);
	}

	/**
	 * Define validation logic in this method. This method is being performed
	 * when validate() method is called.
	 *
	 * @param obj
	 *            - object to validate.
	 */
	protected abstract void doValidateObject(T obj);

	protected void checkNotifications() throws ValidatorException {
		if (!notificationContainer.getBusinessNotifications().isEmpty()
				|| !notificationContainer.getInputNotifications().isEmpty()) {
			throw new ValidatorException(notificationContainer);
		}
	}

	/**
	 * Add Business notification to Notification container.
	 * 
	 * @param notificationMessageProperty
	 *            - notification message name defined in error-messages folder.
	 */
	protected void addBusinessNotification(String notificationMessageProperty) {
		notificationContainer.addBusinessNotification(notificationMessageProperty);
	}
	
	/**
	 * Throw Application error Notification container.
	 * 
	 * @param notificationMessageProperty
	 *            - notification message name defined in error-messages folder.
	 */
	protected void addApplicationErrorNotification() {
		notificationContainer.addBusinessNotification(ValidatorMessages.UNEXPECTED_APPLICATION_ERROR);
	}

	/**
	 * Add Business notification to Notification container.
	 * 
	 * @param notificationMessageProperty
	 *            - notification message name defined in error-messages folder.
	 * @param args
	 *            - optional arguments to bind into notification message. Can be
	 *            null.
	 */
	protected void addBusinessNotification(String notificationMessageProperty, Object... args) {
		notificationContainer.addBusinessNotification(notificationMessageProperty, args);
	}

	protected final FieldValidationHandler inputValidation() {
		return fieldValidator;
	}

	protected final boolean isNotNull(Object obj) {
		return obj != null;
	}

	protected final boolean isNotEmpty(String obj) {
		return obj != null && !obj.isEmpty();
	}

}
