package szczepanski.gerard.betty.service.round;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import szczepanski.gerard.betty.service.values.DomainId;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public final class RoundCreationDto {
	
	private DomainId seasonId;
	private List<RoundFixtureCreationDto> roundFixturesToCreate;
	
	@Override
	public String toString() {
		return "RoundCreationDto [seasonId=" + seasonId + ", roundFixturesToCreate=" + roundFixturesToCreate + "]";
	}
}	
