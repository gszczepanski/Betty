package szczepanski.gerard.betty.service.common.validator;

import java.time.LocalDate;
import java.util.Collection;
import java.util.regex.Pattern;

public class FieldValidationHandler {

	private final NotificationContainer notificationContainer;
	private Pattern pattern;

	private static final String NUMBER_PATTERN = "[0-9.,]+";
	private static final String INTEGER_NUMBER_PATTERN = "[0-9]+";
	private static final String DECIMAL_NUMBER_PATTERN = "[0-9]+(\\.[0-9]{1,2})?";

	private static final String DATE_PATTERN = "^(?:(?:31(\\/)(?:0?[13578]|1[02]))"
			+ "\\1|(?:(?:29|30)(\\/)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$"
			+ "|^(?:29(\\/)(?:0?2)\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$"
			+ "|^(?:0?[1-9]|1\\d|2[0-8])(\\/)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})";

	private static final String YEAR_PATTERN = "[1-2]{1}[0-9]{3}";

	public FieldValidationHandler(NotificationContainer notificationContainer) {
		this.notificationContainer = notificationContainer;
	}

	public void checkNull(String fieldName, Object obj) {
		if (obj == null) {
			notificationContainer.addInputNotification(ValidatorMessages.EMPTY_FIELD, fieldName);
		}
	}

	public void checkEmpty(String fieldName, Object obj) {
		if (obj == null || obj.toString().isEmpty()) {
			notificationContainer.addInputNotification(ValidatorMessages.EMPTY_FIELD, fieldName);
		}
	}

	public void checkIfEqual(String fieldName, String secondFieldName, Object obj, Object obj2) {
		if (!obj.equals(obj2)) {
			//rejectFieldError(fieldName, VALUES_ARE_NOT_EQUAL);
		}
	}

	public void checkIfDifferent(String fieldName, Object obj, Object obj2) {
		if (obj.equals(obj2)) {
			//rejectFieldError(fieldName, VALUES_ARE_NOT_DIFFERENT);
		}
	}

	public void checkUnique(String fieldName, boolean isUnique) {
		if (!isUnique) {
			//rejectFieldError(fieldName, VALUE_IS_OCCUPIED);
		}
	}

	public void checkMinSize(String fieldName, Object obj, int minSize) {
		if (obj.toString().length() < minSize) {
			notificationContainer.addInputNotification(ValidatorMessages.MIN_LENGTH_FIELD, fieldName, String.valueOf(minSize));
		}
	}

	public void checkMaxSize(String fieldName, Object obj, int maxSize) {
		if (obj != null && obj.toString().length() > maxSize) {
			notificationContainer.addInputNotification(ValidatorMessages.MIN_LENGTH_FIELD, fieldName, String.valueOf(maxSize));
		}
	}

	public void checkMinValue(String fieldName, Number number, int minValue) {
		if (number != null && number.doubleValue() < minValue) {
			notificationContainer.addInputNotification(fieldName, String.valueOf(""), String.valueOf(minValue));
		}
	}

	public void checkMaxValue(String fieldName, Number number, int maxValue) {
		if (number != null && number.doubleValue() > maxValue) {
			notificationContainer.addInputNotification(fieldName, String.valueOf(""), String.valueOf(maxValue));
		}
	}

	public void checkMaxFileSize(String fieldName, Object obj, int maxSize) {
		if (obj != null && ((byte[]) obj).length > maxSize) {
			notificationContainer.addInputNotification(fieldName, String.valueOf(""), String.valueOf(maxSize));
		}
	}

	public void checkIfCollectionNotEmpty(String fieldName, Collection<?> collection) {
		if (collection == null || collection.isEmpty()) {
			notificationContainer.addInputNotification(fieldName, "");
		}
	}

	public void checkIfDate(String fieldName, String date) {
		pattern = Pattern.compile(DATE_PATTERN);
		if (!matchValue(date)) {
			notificationContainer.addInputNotification(fieldName, "");
		}
	}

	public void checkIfFutureDate(String fieldName, LocalDate date) {
		LocalDate today = LocalDate.now();
		if (date != null && date.isAfter(today)) {
			notificationContainer.addInputNotification(fieldName, "");
		}
	}

	public void checkIfYear(String fieldName, String year) {
		pattern = Pattern.compile(YEAR_PATTERN);
		if (!matchValue(year)) {
			notificationContainer.addInputNotification(fieldName, "");
		}
	}

	public void checkIfNumber(String fieldName, CharSequence number) {
		pattern = Pattern.compile(NUMBER_PATTERN);
		if (!matchValue(number)) {
			notificationContainer.addInputNotification(fieldName, "");
		}
	}

	public void checkIfIntegerNumber(String fieldName, CharSequence number) {
		pattern = Pattern.compile(INTEGER_NUMBER_PATTERN);
		if (!matchValue(number)) {
			notificationContainer.addInputNotification(fieldName, "");
		}
	}

	public void checkIfDecimalNumber(String fieldName, CharSequence number) {
		pattern = Pattern.compile(DECIMAL_NUMBER_PATTERN);
		if (!matchValue(number)) {
			notificationContainer.addInputNotification(ValidatorMessages.DECIMAL_FIELD, fieldName);
		}
	}

	private boolean matchValue(CharSequence value) {
		return pattern.matcher(value).matches();
	}

	public void matchPattern(String fieldName, Object value, Pattern valuePattern) {
		if (value != null && !matchByRegExp(value, valuePattern)) {
			notificationContainer.addInputNotification(fieldName, "");
		}
	}

	private boolean matchByRegExp(Object obj, Pattern valuePattern) {
		return valuePattern.matcher(String.valueOf(obj)).matches();
	}

	public void checkMaxDate(String fieldName, LocalDate date, LocalDate maxDate) {
		if (date.isAfter(maxDate)) {
			//notificationContainer.addInputNotification(fieldName, "" + EXCEEDED_MAX_DATE, new Object[] { String.valueOf(maxDate.toString()) });
		}
	}

	public void checkMinDate(String fieldName, LocalDate date, LocalDate minDate) {
		if (date.isBefore(minDate)) {
			//notificationContainer.addInputNotification(fieldName, "" + EXCEEDED_MIN_DATE, new Object[] { String.valueOf(minDate.toString()) });
		}
	}
}
