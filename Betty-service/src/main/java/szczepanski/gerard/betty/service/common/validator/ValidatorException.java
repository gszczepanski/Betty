package szczepanski.gerard.betty.service.common.validator;

import lombok.Getter;
import szczepanski.gerard.betty.common.exception.BettyException;

@Getter
public class ValidatorException extends BettyException {
	private static final long serialVersionUID = 4400292708586850469L;

	private final transient NotificationContainer notificationContainer;

	public ValidatorException(NotificationContainer notificationContainer) {
		this.notificationContainer = notificationContainer;
	}

}
