package szczepanski.gerard.betty.service.round;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import szczepanski.gerard.betty.domain.league.round.RoundCodeGenerator;
import szczepanski.gerard.betty.domain.league.round.RoundFixtureCodeGenerator;
import szczepanski.gerard.betty.domain.league.round.RoundQueryRepository;
import szczepanski.gerard.betty.integration.service.fixture.FixtureIntegrationService;
import szczepanski.gerard.betty.integration.service.fixture.IntegrationFixtureSpringConfiguration;
import szczepanski.gerard.betty.integration.service.team.IntegrationTeamSpringConfiguration;
import szczepanski.gerard.betty.integration.service.team.TeamIntegrationService;

@Configuration
@Import(value = {IntegrationFixtureSpringConfiguration.class, IntegrationTeamSpringConfiguration.class})
public class RoundServiceSpringConfiguration {
	
	@Autowired
	private FixtureIntegrationService fixtureIntegrationService;
	
	@Autowired
	private TeamIntegrationService teamIntegrationService;
	
	@Bean
	public RoundFactory roundFactory() {
		return RoundFactory.builder()
				.roundCodeGenerator(roundCodeGenerator())
				.roundFixtureFactory(roundFixtureFactory())
				.build();
	}
	
	@Bean
	public RoundCodeGenerator roundCodeGenerator() {
		return RoundCodeGenerator.builder()
				.roundQueryRepository(roundQueryRepository())
				.build();
	}
	
	@Bean 
	public RoundQueryRepository roundQueryRepository() {
		return null;
	}
	
	@Bean
	public RoundFixtureFactory roundFixtureFactory() {
		return RoundFixtureFactory.builder()
		.fixtureIntegrationService(fixtureIntegrationService)		
		.teamIntegrationService(teamIntegrationService)
		.roundFixtureCodeGenerator(roundFixtureCodeGenerator())
		.roundFixtureFactoryCreationDataValidator(roundFixtureFactoryCreationDataValidator())
		.build();
	}
	
	@Bean
	public RoundFixtureCodeGenerator roundFixtureCodeGenerator() {
		return RoundFixtureCodeGenerator.builder()
				.roundQueryRepository(roundQueryRepository())
				.build();
	}
	
	@Bean
	public RoundFixtureFactoryCreationDataValidator roundFixtureFactoryCreationDataValidator() {
		return new RoundFixtureFactoryCreationDataValidator();
	}
	
	@Bean
	public FinishedRoundCalculator finishedRoundCalculator() {
		return new FinishedRoundCalculator();
	}
	
	@Bean
	public FinishedRoundFinder finishedRoundFinder() {
		return FinishedRoundFinder.builder()
				.roundQueryRepository(roundQueryRepository())
				.build();
	}
	
	@Bean
	public FinishedRoundResultFetcher finishedRoundResultFetcher() {
		return new FinishedRoundResultFetcher(fixtureIntegrationService);
	}
	
}
