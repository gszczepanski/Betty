package szczepanski.gerard.betty.service.bet;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.betty.domain.league.round.RoundQueryRepository;
import szczepanski.gerard.betty.service.common.validator.AbstractValidator;

@Builder
@RequiredArgsConstructor
class BetRoundValidator extends AbstractValidator<BetRoundDto> {

	private final RoundQueryRepository roundQueryRepository;
	
	@Override
	protected void doValidateObject(BetRoundDto obj) {
		
	}
	
}
