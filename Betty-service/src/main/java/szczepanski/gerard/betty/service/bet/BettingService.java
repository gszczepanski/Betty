package szczepanski.gerard.betty.service.bet;

import szczepanski.gerard.betty.service.common.validator.ValidatorException;
import szczepanski.gerard.betty.service.values.Code;

public interface BettingService {
	
	Code betRound(BetRoundDto betDto) throws ValidatorException;
	
}
