package szczepanski.gerard.betty.service.round;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import szczepanski.gerard.betty.integration.service.value.IntegrationId;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public final class RoundFixtureCreationDto {
	
	private IntegrationId fixtureIntegrationId;
	private String fixtureType;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fixtureIntegrationId == null) ? 0 : fixtureIntegrationId.hashCode());
		result = prime * result + ((fixtureType == null) ? 0 : fixtureType.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RoundFixtureCreationDto other = (RoundFixtureCreationDto) obj;
		if (fixtureIntegrationId == null) {
			if (other.fixtureIntegrationId != null)
				return false;
		} else if (!fixtureIntegrationId.equals(other.fixtureIntegrationId))
			return false;
		if (fixtureType == null) {
			if (other.fixtureType != null)
				return false;
		} else if (!fixtureType.equals(other.fixtureType))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CreateRoundFixtureDto [fixtureIntegrationId=" + fixtureIntegrationId + ", fixtureType=" + fixtureType + "]";
	}
	
}
