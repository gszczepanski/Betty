package szczepanski.gerard.betty.service.common.validator;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public final class Notification {

	private final String messageProperty;
	private final Object[] args;
	
	public String getMessage() {
		return String.format(messageProperty, args);
	}
	
}