package szczepanski.gerard.betty.service.values;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.betty.common.assertion.ParamAssertion;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class Code {
	
	private final String code;
	
	public static Code of(String code) {
		ParamAssertion.isNotBlank(code);
		return new Code(code);
	}
	
	public String get() {
		return code;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Code other = (Code) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return code;
	}
	
}
