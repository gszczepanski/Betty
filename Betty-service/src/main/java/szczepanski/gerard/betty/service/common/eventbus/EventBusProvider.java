package szczepanski.gerard.betty.service.common.eventbus;

import com.google.common.eventbus.EventBus;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EventBusProvider {
	
	private static final EventBus EVENT_BUS = new EventBus();
	
	public static EventBus get() {
		return EVENT_BUS;
	}
	
}
