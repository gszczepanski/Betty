package szczepanski.gerard.betty.service.league;

import java.util.Set;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public final class LeagueCreationDto {
	
	private String leagueName;
	private Set<Long> playersIds;
	
}
