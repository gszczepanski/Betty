package szczepanski.gerard.betty.service.round;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.betty.common.message.MessageResolver;
import szczepanski.gerard.betty.domain.league.round.RoundQueryRepository;
import szczepanski.gerard.betty.domain.league.round.model.RoundFixtureType;
import szczepanski.gerard.betty.domain.league.season.SeasonQueryRepository;
import szczepanski.gerard.betty.integration.service.value.IntegrationId;
import szczepanski.gerard.betty.service.common.validator.AbstractValidator;
import szczepanski.gerard.betty.service.league.LeagueOwnershipChecker;
import szczepanski.gerard.betty.service.values.DomainId;

@RequiredArgsConstructor
@Builder
class RoundCreationValidator extends AbstractValidator<RoundCreationDto> {

	private final LeagueOwnershipChecker leagueOwnershipChecker;
	private final RoundQueryRepository roundQueryRepository;
	private final SeasonQueryRepository seasonQueryRepository;

	@Override
	protected void doValidateObject(RoundCreationDto obj) {
		DomainId seasonId = obj.getSeasonId();
		List<RoundFixtureCreationDto> roundFixturesToCreate = obj.getRoundFixturesToCreate();
		
		inputValidation().checkNull(MessageResolver.resolveMessage("season"), seasonId);
		
		if (seasonId != null) {
			validateIsOwnerAction(seasonId);
			validateIfAnyRoundInTheSeasonIsNotFinished(seasonId);
			validateIfCanAddNewRound(seasonId);
		}
		
		inputValidation().checkIfCollectionNotEmpty(MessageResolver.resolveMessage("fixtures"), roundFixturesToCreate);
		
		if (roundFixturesToCreate != null && !roundFixturesToCreate.isEmpty()) {
			validateFixturesToCreate(roundFixturesToCreate);
		}
	}
	
	private void validateIsOwnerAction(DomainId seasonId) {
		Boolean isOwnerOfLeagueAction = leagueOwnershipChecker.isOwnerOfSeasonAction(seasonId);
		if (!isOwnerOfLeagueAction) {
			addBusinessNotification("round.creation.you.do.not.have.access.to.add.round.to.that.league");
		}
	}
	
	private void validateIfAnyRoundInTheSeasonIsNotFinished(DomainId seasonId) {
		Boolean allRoundsAreFinishedForSeason = roundQueryRepository.isAllRoundsAreFinishedForSeason(seasonId.get());
		if (!allRoundsAreFinishedForSeason) {
			addBusinessNotification("round.creation.all.rounds.in.current.season.are.not.finished");
		}
	}
	
	private void validateIfCanAddNewRound(DomainId seasonId) {
		Boolean canAddNewRound = seasonQueryRepository.canAddNewRound(seasonId.get());
		if (!canAddNewRound) {
			addBusinessNotification("round.creation.season.is.already.finished");
		}
	}
	
	private void validateFixturesToCreate(List<RoundFixtureCreationDto> roundFixturesToCreate) {
		for (RoundFixtureCreationDto roundFixtureCreationDto: roundFixturesToCreate) {
			
			IntegrationId fixtureIntegrationId = roundFixtureCreationDto.getFixtureIntegrationId();
			String fixtureType = roundFixtureCreationDto.getFixtureType();
			
			if (fixtureIntegrationId == null) {
				addBusinessNotification("round.creation.round.fixture.to.create.is.not.valid");
				return;
			}
			
			if (fixtureType == null || !RoundFixtureType.canBeMappedFromString(fixtureType)) {
				addBusinessNotification("round.creation.round.fixture.to.create.is.not.valid");
				return;
			}
		}
		
		validateFixturesUniqueIds(roundFixturesToCreate);
	}
	
	private void validateFixturesUniqueIds(List<RoundFixtureCreationDto> roundFixturesToCreate) {
		Set<IntegrationId> uniqueIds = roundFixturesToCreate.stream()
													.map(RoundFixtureCreationDto::getFixtureIntegrationId)
													.collect(Collectors.toSet());
		
		if (uniqueIds.size() != roundFixturesToCreate.size()) {
			addBusinessNotification("round.creation.round.fixture.to.create.is.not.valid");
		}
	}

}
