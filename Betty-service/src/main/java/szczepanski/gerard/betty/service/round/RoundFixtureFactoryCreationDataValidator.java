package szczepanski.gerard.betty.service.round;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;

import szczepanski.gerard.betty.integration.service.fixture.IntegrationFixture;
import szczepanski.gerard.betty.integration.service.team.IntegrationTeam;
import szczepanski.gerard.betty.integration.service.value.IntegrationId;
import szczepanski.gerard.betty.service.common.validator.AbstractValidator;
import szczepanski.gerard.betty.service.round.RoundFixtureFactory.RoundFixtureFactoryCreationData;

public class RoundFixtureFactoryCreationDataValidator extends AbstractValidator<RoundFixtureFactoryCreationData>{
	
	private static final String BUSINESS_ERROR_MSG = "round.creation.could.not.obtain.data.for.fixtures.please.try.again.later";
	private static final Long DAYS_OFFSET = 1L;

	@Override
	protected void doValidateObject(RoundFixtureFactoryCreationData obj) {
		List<RoundFixtureCreationDto> creationDtos = obj.getCreationDtos();
		Map<IntegrationId, IntegrationFixture> integrationFixturesMap = obj.getIntegrationFixturesMap();
		Map<IntegrationId, IntegrationTeam> integrationTeamsMap = obj.getIntegrationTeamsMap();
		
		for(RoundFixtureCreationDto creationDto: creationDtos) {
			IntegrationId fixtureIntegrationId = creationDto.getFixtureIntegrationId();
			
			if (!integrationFixturesMap.containsKey(fixtureIntegrationId)) {
				addBusinessNotification(BUSINESS_ERROR_MSG);
				return;
			}
			
			IntegrationFixture integrationFixture = integrationFixturesMap.get(fixtureIntegrationId);
			validateIfFixtureIsAtLeastOneDayFuture(integrationFixture);
			
			if (!integrationTeamsMap.containsKey(integrationFixture.getHomeTeamId()) || !integrationTeamsMap.containsKey(integrationFixture.getAwayTeamId())) {
				addBusinessNotification(BUSINESS_ERROR_MSG);
				return;
			}
		}
	}
	
	private void validateIfFixtureIsAtLeastOneDayFuture(IntegrationFixture integrationFixture) {
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime startDate = integrationFixture.getStartDate();
		
		long daysUntilFixture = now.until(startDate, ChronoUnit.DAYS);
		
		if (daysUntilFixture < DAYS_OFFSET) {
			addBusinessNotification("round.creation.time.to.match.is.less.than.one.day=", integrationFixture.getId().get());
		}
	}
	
}
