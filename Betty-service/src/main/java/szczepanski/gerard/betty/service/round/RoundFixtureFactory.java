package szczepanski.gerard.betty.service.round;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.betty.common.assertion.ParamAssertion;
import szczepanski.gerard.betty.common.logger.BettyLogger;
import szczepanski.gerard.betty.common.logger.BettyLoggerFactory;
import szczepanski.gerard.betty.domain.league.round.RoundFixtureCodeGenerator;
import szczepanski.gerard.betty.domain.league.round.model.FixtureTeam;
import szczepanski.gerard.betty.domain.league.round.model.RoundFixture;
import szczepanski.gerard.betty.domain.league.round.model.RoundFixtureType;
import szczepanski.gerard.betty.integration.service.fixture.FixtureIntegrationService;
import szczepanski.gerard.betty.integration.service.fixture.IntegrationFixture;
import szczepanski.gerard.betty.integration.service.team.IntegrationTeam;
import szczepanski.gerard.betty.integration.service.team.TeamIntegrationService;
import szczepanski.gerard.betty.integration.service.value.IntegrationId;
import szczepanski.gerard.betty.service.common.validator.ValidatorException;

/**
 * Creates RoundFixture entities for newly created Round.
 * 
 * @author Gerard Szczepanski
 */
@Builder
@RequiredArgsConstructor
class RoundFixtureFactory {
	private static final BettyLogger LOG = BettyLoggerFactory.createForName(RoundFixtureFactory.class.getName());
	
	private final FixtureIntegrationService fixtureIntegrationService;
	private final TeamIntegrationService teamIntegrationService;
	private final RoundFixtureFactoryCreationDataValidator roundFixtureFactoryCreationDataValidator;
	private final RoundFixtureCodeGenerator roundFixtureCodeGenerator;
	
	public Set<RoundFixture> createRoundFixtures(List<RoundFixtureCreationDto> creationDtos) throws ValidatorException {
		ParamAssertion.collectionNotEmpty(creationDtos);
		LOG.debug("Starting to create RoundFixtures for given dtos: %s", creationDtos);
		
		RoundFixtureFactoryCreationData creationData = createCreationData(creationDtos);
		roundFixtureFactoryCreationDataValidator.validate(creationData);
		
		return assemblyFromCreationData(creationData);
	}
	
	private RoundFixtureFactoryCreationData createCreationData(List<RoundFixtureCreationDto> creationDtos) {
		Map<IntegrationId, IntegrationFixture> fixturesMapFromIntegration = getFixturesMapFromIntegration(creationDtos);
		Map<IntegrationId, IntegrationTeam> teamsMapFromIntegration = getTeamsMapFromIntegration(fixturesMapFromIntegration);
		
		return RoundFixtureFactoryCreationData.builder()
				.creationDtos(creationDtos)
				.integrationFixturesMap(fixturesMapFromIntegration)
				.integrationTeamsMap(teamsMapFromIntegration)
				.build();
	}
	
	private Map<IntegrationId, IntegrationFixture> getFixturesMapFromIntegration(List<RoundFixtureCreationDto> creationDtos) {
		List<IntegrationId> fixturesIds = creationDtos.stream().map(RoundFixtureCreationDto::getFixtureIntegrationId).collect(Collectors.toList());
		Set<IntegrationFixture> fixturesFound = fixtureIntegrationService.findMany(fixturesIds);
		return fixturesFound.stream().collect(Collectors.toMap(IntegrationFixture::getId, Function.identity()));
	}
	
	private Map<IntegrationId, IntegrationTeam> getTeamsMapFromIntegration(Map<IntegrationId, IntegrationFixture> fixturesMapFromIntegration) {
		Set<IntegrationId> uniqueTeamIds = new HashSet<>();
		
		fixturesMapFromIntegration.values().forEach(f -> {
			uniqueTeamIds.add(f.getHomeTeamId());
			uniqueTeamIds.add(f.getAwayTeamId());
		});
		
		List<IntegrationId> teamIds = uniqueTeamIds.stream().collect(Collectors.toList());
		Set<IntegrationTeam> teamsFound = teamIntegrationService.findMany(teamIds);
		return teamsFound.stream().collect(Collectors.toMap(IntegrationTeam::getId, Function.identity()));
	}
	
	private Set<RoundFixture> assemblyFromCreationData(RoundFixtureFactoryCreationData creationData) {
		List<RoundFixtureCreationDto> creationDtos = creationData.getCreationDtos();
		Map<IntegrationId, IntegrationFixture> integrationFixturesMap = creationData.getIntegrationFixturesMap();
		Map<IntegrationId, IntegrationTeam> integrationTeamsMap = creationData.getIntegrationTeamsMap();
		
		List<String> nextCodes = roundFixtureCodeGenerator.nextCodes(creationDtos.size());
		
		Set<RoundFixture> assembledFixtures = new HashSet<>(creationDtos.size());
		for (int order = 1; order <= creationDtos.size(); order++) {
			int index = order - 1;
			RoundFixtureCreationDto fixtureCreationDto = creationDtos.get(index);
			
			IntegrationId fixtureIntegrationId = fixtureCreationDto.getFixtureIntegrationId();
			IntegrationFixture integrationFixture = integrationFixturesMap.get(fixtureIntegrationId);
			IntegrationTeam integrationHomeTeam = integrationTeamsMap.get(integrationFixture.getHomeTeamId());
			IntegrationTeam integrationAwayTeam = integrationTeamsMap.get(integrationFixture.getAwayTeamId());
			
			LocalDateTime startDate = integrationFixture.getStartDate();
			LocalDateTime endDate = FixtureEndDateCalculator.getFixtureEndDate(startDate);
			
			RoundFixture fixtureCreated = RoundFixture.builder()
						.code(nextCodes.get(index))
						.synchronizationFixtureId(fixtureIntegrationId.get())
						.type(RoundFixtureType.valueOf(fixtureCreationDto.getFixtureType()))
						.order(order)
						.homeTeam(assemblyTeam(integrationHomeTeam))
						.awayTeam(assemblyTeam(integrationAwayTeam))
						.startDate(startDate)
						.endDate(endDate)
						.build();
			
			assembledFixtures.add(fixtureCreated);
		}
		
		return assembledFixtures;
	}
	
	private FixtureTeam assemblyTeam(IntegrationTeam integrationTeam) {
		return FixtureTeam.builder()
				.synchronizationTeamId(integrationTeam.getId().get())
				.name(integrationTeam.getName())
				.imagePath(integrationTeam.getImageURL())
				.build();
	}
	
	@Getter
	@RequiredArgsConstructor
	@Builder
	static class RoundFixtureFactoryCreationData {
		private final List<RoundFixtureCreationDto> creationDtos;
		private final Map<IntegrationId, IntegrationFixture> integrationFixturesMap;
		private final Map<IntegrationId, IntegrationTeam> integrationTeamsMap;
	}
	
}
