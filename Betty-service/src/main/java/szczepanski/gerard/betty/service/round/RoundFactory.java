package szczepanski.gerard.betty.service.round;

import java.time.LocalDateTime;
import java.util.Set;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.betty.common.logger.BettyLogger;
import szczepanski.gerard.betty.common.logger.BettyLoggerFactory;
import szczepanski.gerard.betty.domain.league.round.RoundCodeGenerator;
import szczepanski.gerard.betty.domain.league.round.model.Round;
import szczepanski.gerard.betty.domain.league.round.model.RoundFixture;
import szczepanski.gerard.betty.domain.league.round.model.RoundState;
import szczepanski.gerard.betty.domain.league.season.model.Season;
import szczepanski.gerard.betty.service.common.validator.ValidatorException;

@RequiredArgsConstructor
@Builder
class RoundFactory {
	private static final BettyLogger LOG = BettyLoggerFactory.createForName(RoundFactory.class.getName());

	private final RoundFixtureFactory roundFixtureFactory;
	private final RoundCodeGenerator roundCodeGenerator;

	public Round createRound(RoundCreationDto roundCreationDto) throws ValidatorException {
		LOG.debug("Create Round from dto: %s", roundCreationDto);
		Set<RoundFixture> roundFixtures = roundFixtureFactory.createRoundFixtures(roundCreationDto.getRoundFixturesToCreate());
		return assemblyRound(roundCreationDto, roundFixtures);
	}

	private Round assemblyRound(RoundCreationDto roundCreationDto, Set<RoundFixture> roundFixtures) {
		LocalDateTime roundStartDate = getStartDate(roundFixtures);
		LocalDateTime roundFinishDate = getFinishDate(roundFixtures);

		Season season = new Season(roundCreationDto.getSeasonId().get());
		return Round.builder()
					.season(season)
					.code(roundCodeGenerator.nextCode())
					.roundFixtures(roundFixtures)
					.state(RoundState.OPEN)
					.startDate(roundStartDate)
					.finishDate(roundFinishDate)
					.build();
	}

	private LocalDateTime getStartDate(Set<RoundFixture> roundFixtures) {
		LocalDateTime earliestDate = null;

		for (RoundFixture fixture : roundFixtures) {
			LocalDateTime fixtureStartDate = fixture.getStartDate();

			if (earliestDate == null) {
				earliestDate = fixtureStartDate;
			} else {
				if (earliestDate.isAfter(fixtureStartDate)) {
					earliestDate = fixtureStartDate;
				}
			}
		}
		return earliestDate;
	}

	private LocalDateTime getFinishDate(Set<RoundFixture> roundFixtures) {
		LocalDateTime latestDate = null;

		for (RoundFixture fixture : roundFixtures) {
			LocalDateTime fixtureEndDate = fixture.getEndDate();

			if (latestDate == null) {
				latestDate = fixtureEndDate;
			} else {
				if (latestDate.isBefore(fixtureEndDate)) {
					latestDate = fixtureEndDate;
				}
			}
		}
		return latestDate;
	}
}
