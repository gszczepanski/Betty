/**
 * 
 */
package szczepanski.gerard.betty.service.common.validator;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ValidatorMessages {
	
	public static final String UNEXPECTED_APPLICATION_ERROR = "Unexpected Application error";
	
	public static final String EMPTY_FIELD = "Please correct %s field, because it is empty";
	public static final String MIN_LENGTH_FIELD = "%s field must have at least %s characters";
	public static final String MAX_LENGTH_FIELD = "%s field must have no more than %s characters";
	public static final String DECIMAL_FIELD = "%s field decimal value is not valid";
}
