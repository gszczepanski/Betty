/**
 * 
 */
package szczepanski.gerard.betty.service.common.validator;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ValidationRequirements {
	
	public static final int MIN_TEXT_INPUT_LENGTH = 3;
	public static final int MAX_TEXT_INPUT_LENGTH = 50;
	
	public static final int MIN_PASSWORD_LENGTH = 6;
	public static final int MAX_PASSWORD_LENGTH = MAX_TEXT_INPUT_LENGTH;
	
	public static final int MIN_USERNAME_LENGTH = 3;
	public static final int MAX_USERNAME_LENGTH = MAX_TEXT_INPUT_LENGTH;
	
	
}
