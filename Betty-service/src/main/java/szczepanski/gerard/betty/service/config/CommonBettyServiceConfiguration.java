package szczepanski.gerard.betty.service.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import szczepanski.gerard.betty.domain.config.PersistenceJpaConfiguration;

@Configuration
@Import(PersistenceJpaConfiguration.class)
public class CommonBettyServiceConfiguration {

}
