package szczepanski.gerard.betty.service.bet;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.betty.common.logger.BettyLogger;
import szczepanski.gerard.betty.common.logger.BettyLoggerFactory;
import szczepanski.gerard.betty.domain.league.bet.BetCodeGenerator;
import szczepanski.gerard.betty.domain.league.bet.RoundBetCodeGenerator;
import szczepanski.gerard.betty.domain.league.bet.model.Bet;
import szczepanski.gerard.betty.domain.league.bet.model.RoundBet;
import szczepanski.gerard.betty.domain.league.round.model.Round;
import szczepanski.gerard.betty.domain.league.round.model.RoundFixture;
import szczepanski.gerard.betty.domain.league.season.SeasonPlayerCardQueryRepository;
import szczepanski.gerard.betty.domain.league.season.model.SeasonPlayerCard;
import szczepanski.gerard.betty.integration.service.user.CurrentBettyUserService;
import szczepanski.gerard.betty.service.bet.BetRoundDto.BetFixtureDto;
import szczepanski.gerard.betty.service.values.DomainId;

@Builder
@RequiredArgsConstructor
class RoundBetFactory {
	private static final BettyLogger LOG = BettyLoggerFactory.createForName(RoundBetFactory.class.getName());
	
	private final CurrentBettyUserService currentBettyUserService;
	private final SeasonPlayerCardQueryRepository seasonPlayerCardQueryRepository;
	private final RoundBetCodeGenerator roundBetCodeGenerator;
	private final BetCodeGenerator betCodeGenerator;
	
	public RoundBet createRoundBet(BetRoundDto betRoundDto) {
		LOG.debug("Create For Round with id: %s. BetRoundDto: %s", betRoundDto.getRoundId(), betRoundDto);
		
		String roundBetCode = roundBetCodeGenerator.nextCode();
		Round roundForRoundBet = new Round(betRoundDto.getRoundId().get());
		SeasonPlayerCard playerCard = getSeasonPlayerCard();
		Set<Bet> bets = createBets(betRoundDto.getBetFixtures());
		
		return RoundBet.builder()
				.code(roundBetCode)
				.playerCard(playerCard)
				.forRound(roundForRoundBet)
				.bets(bets)
				.build();
	}
	
	private SeasonPlayerCard getSeasonPlayerCard() {
		DomainId currentUserId = currentBettyUserService.getCurrentUserId();
		return seasonPlayerCardQueryRepository.getPlayerCardByBettyUserId(currentUserId.get());
	}
	
	private Set<Bet> createBets(Set<BetFixtureDto> betFixtures) {
		int fixturesSize = betFixtures.size();
		List<String> codesForBets = betCodeGenerator.nextCodes(fixturesSize);
		int betCounter = 0; 
		
		Set<Bet> bets = new HashSet<>(fixturesSize);
		
		for (BetFixtureDto betFixtureDto: betFixtures) {
			String betCode = codesForBets.get(betCounter);
			Bet bet = createBet(betCode, betFixtureDto);
			bets.add(bet);
			betCounter++;
		}
		
		return bets;
	}
	
	private Bet createBet(String betCode, BetFixtureDto betFixtureDto) {
		RoundFixture roundFixture = new RoundFixture(betFixtureDto.getRoundFixtureId().get());
		return Bet.builder()
				.code(betCode)
				.homeScoreBet(betFixtureDto.getHomeGoals())
				.awayScoreBet(betFixtureDto.getAwayGoals())
				.fixture(roundFixture)
				.build();
	}
}
