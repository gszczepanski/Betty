package szczepanski.gerard.betty.service.league;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.betty.domain.league.LeagueQueryRepository;
import szczepanski.gerard.betty.service.values.DomainId;


@RequiredArgsConstructor
@Builder
public class LeagueOwnershipChecker {
	
	private final LeagueQueryRepository leagueQueryRepository;

	public Boolean isOwnerOfLeagueAction(DomainId leagueId) {
		
		
		return false;
	}

	public Boolean isOwnerOfSeasonAction(DomainId seasonId) {

		return false;
	}

}
