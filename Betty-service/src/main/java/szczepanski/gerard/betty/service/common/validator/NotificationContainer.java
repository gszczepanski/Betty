package szczepanski.gerard.betty.service.common.validator;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

@Getter
public class NotificationContainer {

	private final List<Notification> inputNotifications;

	private final List<Notification> businessNotifications;

	public NotificationContainer() {
		inputNotifications = new ArrayList<>();
		businessNotifications = new ArrayList<>();
	}

	public void addBusinessNotification(String notificationMessageProperty, Object... args) {
		businessNotifications.add(new Notification(notificationMessageProperty, args));
	}

	public void addInputNotification(String notificationMessageProperty, Object... args) {
		inputNotifications.add(new Notification(notificationMessageProperty, args));
	}

}