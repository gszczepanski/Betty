package szczepanski.gerard.betty.service.round;

import java.util.HashSet;
import java.util.Set;

import szczepanski.gerard.betty.common.logger.BettyLogger;
import szczepanski.gerard.betty.common.logger.BettyLoggerFactory;
import szczepanski.gerard.betty.service.common.eventbus.EventBusProvider;
import szczepanski.gerard.betty.service.values.Code;

public class FinishedRoundCalculator {
	private static final BettyLogger LOG = BettyLoggerFactory.createForName(FinishedRoundCalculator.class.getName());
	
	private final Set<Code> finishedRoundsResults; //TODO events with fixture results instead of codes!
	
	public FinishedRoundCalculator() {
		this.finishedRoundsResults = new HashSet<>();
		EventBusProvider.get().register(this);
	}
	
	public void summarizeFinishedRounds() {
		LOG.debug("Starting to summarize finished rounds with codes: %s", finishedRoundsResults);
		finishedRoundsResults.clear();
	}
	
}
