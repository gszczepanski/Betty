package szczepanski.gerard.betty.service.bet;

import org.springframework.transaction.annotation.Transactional;

import lombok.AllArgsConstructor;
import lombok.Builder;
import szczepanski.gerard.betty.common.assertion.ParamAssertion;
import szczepanski.gerard.betty.common.logger.BettyLogger;
import szczepanski.gerard.betty.common.logger.BettyLoggerFactory;
import szczepanski.gerard.betty.domain.league.bet.RoundBetCommandRepository;
import szczepanski.gerard.betty.domain.league.bet.model.RoundBet;
import szczepanski.gerard.betty.service.common.lock.AutoReleaseLock;
import szczepanski.gerard.betty.service.common.validator.ValidatorException;
import szczepanski.gerard.betty.service.values.Code;

@Builder
@AllArgsConstructor
class BettingServiceImpl implements BettingService {
	private static final BettyLogger LOG = BettyLoggerFactory.createForName(BettingService.class.getName());

	private final BetRoundValidator betRoundValidator;
	private final RoundBetFactory betFactory;
	private final RoundBetCommandRepository betCommandRepository;

	@Override
	@Transactional
	public Code betRound(BetRoundDto betDto) throws ValidatorException {
		ParamAssertion.isNotNull(betDto);
		betRoundValidator.validate(betDto);
		LOG.debug("Bet round with dto: %s ", betDto);

		try (AutoReleaseLock lock = AutoReleaseLock.fromReentrantLock()) {
			lock.lock();
			return createRound(betDto);
		}
	}

	private Code createRound(BetRoundDto betDto) {
		RoundBet roundBet = betFactory.createRoundBet(betDto);
		betCommandRepository.create(roundBet);
		return Code.of(roundBet.getCode());
	}

}
