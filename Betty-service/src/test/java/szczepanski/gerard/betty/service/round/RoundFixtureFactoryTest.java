package szczepanski.gerard.betty.service.round;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mockito.internal.util.collections.Sets;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import szczepanski.gerard.betty.common.assertion.ParamAssertionException;
import szczepanski.gerard.betty.domain.league.round.RoundFixtureCodeGenerator;
import szczepanski.gerard.betty.domain.league.round.model.RoundFixture;
import szczepanski.gerard.betty.domain.league.round.model.RoundFixtureType;
import szczepanski.gerard.betty.integration.service.fixture.FixtureIntegrationService;
import szczepanski.gerard.betty.integration.service.fixture.IntegrationFixture;
import szczepanski.gerard.betty.integration.service.team.IntegrationTeam;
import szczepanski.gerard.betty.integration.service.team.TeamIntegrationService;
import szczepanski.gerard.betty.integration.service.value.IntegrationId;
import szczepanski.gerard.betty.service.common.validator.ValidatorException;

@Test(groups = "unit-tests")
public class RoundFixtureFactoryTest {

	FixtureIntegrationService fixtureIntegrationService;
	TeamIntegrationService teamIntegrationService;
	RoundFixtureFactoryCreationDataValidator roundFixtureFactoryCreationDataValidator;
	RoundFixtureCodeGenerator roundFixtureCodeGenerator;
	RoundFixtureFactory roundFixtureFactory;
	
	@BeforeMethod
	public void beforeMethod() {
		fixtureIntegrationService = Mockito.mock(FixtureIntegrationService.class);
		teamIntegrationService = Mockito.mock(TeamIntegrationService.class);
		roundFixtureFactoryCreationDataValidator = Mockito.mock(RoundFixtureFactoryCreationDataValidator.class);
		roundFixtureCodeGenerator = Mockito.mock(RoundFixtureCodeGenerator.class);
		
		roundFixtureFactory = RoundFixtureFactory.builder()
				.fixtureIntegrationService(fixtureIntegrationService)
				.teamIntegrationService(teamIntegrationService)
				.roundFixtureFactoryCreationDataValidator(roundFixtureFactoryCreationDataValidator)
				.roundFixtureCodeGenerator(roundFixtureCodeGenerator)
				.build();
	}
	
	@Test
	public void createRoundFixtuesSuccess() throws Exception {
		// Arrange
		IntegrationId firstFixtureId = IntegrationId.of("100000");
		String firstFixtureType = RoundFixtureType.NORMAL.toString();
		Integer expectedFirstFixtureOrder = 1;
		RoundFixtureCreationDto firstFixture = RoundFixtureCreationDto.builder()
				.fixtureIntegrationId(firstFixtureId)
				.fixtureType(firstFixtureType)
				.build();
		
		IntegrationId secondFixtureId = IntegrationId.of("20000");
		String secondFixtureType = RoundFixtureType.NORMAL.toString();
		Integer expectedSecondFixtureOrder = 2;
		RoundFixtureCreationDto secondFixture = RoundFixtureCreationDto.builder()
				.fixtureIntegrationId(secondFixtureId)
				.fixtureType(secondFixtureType)
				.build();
		
		List<RoundFixtureCreationDto> fixturesToCreate = Arrays.asList(firstFixture, secondFixture);
		List<IntegrationId> fixtureIntegrationIdsToPull = Arrays.asList(firstFixtureId, secondFixtureId);
		
		String firstFixtureGeneratedCode = "FIXTURE-0001";
		String rawFirstFixtureHomeTeamId = "123";
		IntegrationId firstFixtureHomeTeamId = IntegrationId.of(rawFirstFixtureHomeTeamId);
		String rawFirstFixtureAwayTeamId = "124";
		IntegrationId firstFixtureAwayTeamId = IntegrationId.of(rawFirstFixtureAwayTeamId);
		LocalDateTime firstFixtureStartDate = LocalDateTime.of(2017, 8, 14, 12, 00);
		LocalDateTime expectedFirstFixtureEndDate = FixtureEndDateCalculator.getFixtureEndDate(firstFixtureStartDate);
		IntegrationFixture firstIntegrationFixture = IntegrationFixture.builder()
				.id(firstFixtureId)
				.homeTeamId(firstFixtureHomeTeamId)
				.awayTeamId(firstFixtureAwayTeamId)
				.startDate(firstFixtureStartDate)
				.build();
		
		String secondFixtureGeneratedCode = "FIXTURE-0002";
		String rawSecondFixtureHomeTeamId = "23";
		IntegrationId secondFixtureHomeTeamId = IntegrationId.of(rawSecondFixtureHomeTeamId);
		String rawSecondFixtureAwayTeamId = "71";
		IntegrationId secondFixtureAwayTeamId = IntegrationId.of(rawSecondFixtureAwayTeamId);
		LocalDateTime secondFixtureStartDate = LocalDateTime.of(2017, 8, 14, 15, 00);
		LocalDateTime expectedSecondFixtureEndDate = FixtureEndDateCalculator.getFixtureEndDate(secondFixtureStartDate);
		IntegrationFixture secondIntegrationFixture = IntegrationFixture.builder()
				.id(secondFixtureId)
				.homeTeamId(secondFixtureHomeTeamId)
				.awayTeamId(secondFixtureAwayTeamId)
				.startDate(secondFixtureStartDate)
				.build();
		
		Set<IntegrationFixture> returnedIntegrationFixtures = Sets.newSet(firstIntegrationFixture, secondIntegrationFixture);
		Mockito.when(fixtureIntegrationService.findMany(fixtureIntegrationIdsToPull)).thenReturn(returnedIntegrationFixtures);
		
		String firstFixtureHomeTeamName = "Juventus";
		String firstFixtureHomeTeamImageUrl = "http://juventus.com";
		IntegrationTeam firstFixtureHomeTeam = IntegrationTeam.builder()
				.id(firstFixtureHomeTeamId)
				.name(firstFixtureHomeTeamName)
				.imageURL(firstFixtureHomeTeamImageUrl)
				.build();
		
		String firstFixtureAwayTeamName = "Rome";
		String firstFixtureAwayTeamImageUrl = "http://rome.com";
		IntegrationTeam firstFixtureAwayTeam = IntegrationTeam.builder()
				.id(firstFixtureAwayTeamId)
				.name(firstFixtureAwayTeamName)
				.imageURL(firstFixtureAwayTeamImageUrl)
				.build();
		
		String secondFixtureHomeTeamName = "Milan";
		String secondFixtureHomeTeamImageUrl = "http://milan.com";
		IntegrationTeam secondFixtureHomeTeam = IntegrationTeam.builder()
				.id(secondFixtureHomeTeamId)
				.name(secondFixtureHomeTeamName)
				.imageURL(secondFixtureHomeTeamImageUrl)
				.build();
		
		String secondFixtureAwayTeamName = "Inter";
		String secondFixtureAwayTeamImageUrl = "http://inter.com";
		IntegrationTeam secondfirstFixtureAwayTeam = IntegrationTeam.builder()
				.id(secondFixtureAwayTeamId)
				.name(secondFixtureAwayTeamName)
				.imageURL(secondFixtureAwayTeamImageUrl)
				.build();
		
		Set<IntegrationTeam> teamsFromIntegration = Sets.newSet(
												firstFixtureHomeTeam, 
												firstFixtureAwayTeam, 
												secondFixtureHomeTeam, 
												secondfirstFixtureAwayTeam);
		Mockito.when(teamIntegrationService.findMany(Matchers.anyList())).thenReturn(teamsFromIntegration);
		
		int expectedCreatedRoundFixturesSize = 2; 
		
		List<String> generatedCodes = Arrays.asList(firstFixtureGeneratedCode, secondFixtureGeneratedCode);
		Mockito.when(roundFixtureCodeGenerator.nextCodes(expectedCreatedRoundFixturesSize)).thenReturn(generatedCodes);
		
		// Act
		Set<RoundFixture> createdRoundFixtures = roundFixtureFactory.createRoundFixtures(fixturesToCreate);
		
		// Assert
		Assert.assertNotNull(createdRoundFixtures);
		Assert.assertEquals(createdRoundFixtures.size(), expectedCreatedRoundFixturesSize);
		
		List<RoundFixture> orderedRoundFixtures = createdRoundFixtures.stream()
				.sorted((f1, f2) -> f1.getOrder().compareTo(f2.getOrder()))
				.collect(Collectors.toList());
		
		RoundFixture firstCreatedFixture = orderedRoundFixtures.get(0);
		
		Assert.assertNotNull(firstCreatedFixture);
		Assert.assertEquals(firstCreatedFixture.getCode(), firstFixtureGeneratedCode);
		Assert.assertEquals(firstCreatedFixture.getSynchronizationFixtureId(), firstFixtureId.get());
		Assert.assertEquals(firstCreatedFixture.getOrder(), expectedFirstFixtureOrder);
		Assert.assertEquals(firstCreatedFixture.getStartDate(), firstFixtureStartDate);
		Assert.assertEquals(firstCreatedFixture.getEndDate(), expectedFirstFixtureEndDate);
		Assert.assertEquals(firstCreatedFixture.getType(), RoundFixtureType.NORMAL);
		
		Assert.assertNotNull(firstCreatedFixture.getHomeTeam());
		Assert.assertEquals(firstCreatedFixture.getHomeTeam().getSynchronizationTeamId(), rawFirstFixtureHomeTeamId);
		Assert.assertEquals(firstCreatedFixture.getHomeTeam().getName(), firstFixtureHomeTeamName);
		Assert.assertEquals(firstCreatedFixture.getHomeTeam().getImagePath(), firstFixtureHomeTeamImageUrl);
		
		Assert.assertNotNull(firstCreatedFixture.getAwayTeam());
		Assert.assertEquals(firstCreatedFixture.getAwayTeam().getSynchronizationTeamId(), rawFirstFixtureAwayTeamId);
		Assert.assertEquals(firstCreatedFixture.getAwayTeam().getName(), firstFixtureAwayTeamName);
		Assert.assertEquals(firstCreatedFixture.getAwayTeam().getImagePath(), firstFixtureAwayTeamImageUrl);
		
		RoundFixture secondCreatedFixture = orderedRoundFixtures.get(1);
		
		Assert.assertNotNull(secondCreatedFixture);
		Assert.assertEquals(secondCreatedFixture.getCode(), secondFixtureGeneratedCode);
		Assert.assertEquals(secondCreatedFixture.getSynchronizationFixtureId(), secondFixtureId.get());
		Assert.assertEquals(secondCreatedFixture.getOrder(), expectedSecondFixtureOrder);
		Assert.assertEquals(secondCreatedFixture.getStartDate(), secondFixtureStartDate);
		Assert.assertEquals(secondCreatedFixture.getEndDate(), expectedSecondFixtureEndDate);
		Assert.assertEquals(secondCreatedFixture.getType(), RoundFixtureType.NORMAL);
		
		Assert.assertNotNull(secondCreatedFixture.getHomeTeam());
		Assert.assertEquals(secondCreatedFixture.getHomeTeam().getSynchronizationTeamId(), rawSecondFixtureHomeTeamId);
		Assert.assertEquals(secondCreatedFixture.getHomeTeam().getName(), secondFixtureHomeTeamName);
		Assert.assertEquals(secondCreatedFixture.getHomeTeam().getImagePath(), secondFixtureHomeTeamImageUrl);
		
		Assert.assertNotNull(secondCreatedFixture.getAwayTeam());
		Assert.assertEquals(secondCreatedFixture.getAwayTeam().getSynchronizationTeamId(), rawSecondFixtureAwayTeamId);
		Assert.assertEquals(secondCreatedFixture.getAwayTeam().getName(), secondFixtureAwayTeamName);
		Assert.assertEquals(secondCreatedFixture.getAwayTeam().getImagePath(), secondFixtureAwayTeamImageUrl);
		
		Mockito.verify(fixtureIntegrationService).findMany(fixtureIntegrationIdsToPull);
		Mockito.verify(teamIntegrationService).findMany(Matchers.anyList());
		Mockito.verify(roundFixtureFactoryCreationDataValidator).validate(Matchers.any());
		Mockito.verify(roundFixtureCodeGenerator).nextCodes(expectedCreatedRoundFixturesSize);
	}
	
	@Test(expectedExceptions = ParamAssertionException.class)
	public void whenCollectionOfRoundFixtureCreationDtosIsEmptyThenThrowException() throws Exception {
		// Arrange
		List<RoundFixtureCreationDto> EMPTY_COLLECTION = new ArrayList<>();
		
		// Act
		roundFixtureFactory.createRoundFixtures(EMPTY_COLLECTION);
	}
	
	@Test(expectedExceptions = ParamAssertionException.class)
	public void whenCollectionOfRoundFixtureCreationDtosIsNullThenThrowException() throws Exception {
		// Arrange
		List<RoundFixtureCreationDto> NULL_COLLECTION = null;
		
		// Act
		roundFixtureFactory.createRoundFixtures(NULL_COLLECTION);
	}
	
	@Test(expectedExceptions = ValidatorException.class)
	public void whenValidationFailedThenThrowException() throws Exception {
		// Arrange
		IntegrationId firstFixtureId = IntegrationId.of("10");
		String firstFixtureType = RoundFixtureType.NORMAL.toString();
		RoundFixtureCreationDto firstFixture = RoundFixtureCreationDto.builder()
				.fixtureIntegrationId(firstFixtureId)
				.fixtureType(firstFixtureType)
				.build();
		
		List<RoundFixtureCreationDto> roundFixtureCreationDtos = Arrays.asList(firstFixture);
		Mockito.doThrow(ValidatorException.class).when(roundFixtureFactoryCreationDataValidator).validate(Matchers.any());
		
		// Act
		roundFixtureFactory.createRoundFixtures(roundFixtureCreationDtos);
	}
	
	
}
