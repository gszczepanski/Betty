package szczepanski.gerard.betty.service.league;

import org.testng.annotations.Test;

@Test(groups = "unit-tests")
public class LeagueOwnershipCheckerTest {
	/**
	 * This tests wait for Spring security infrastructure
	 */
	
	@Test
	public void whenUserIsOwnerOfLeagueWithGivenLeagueIdThenReturnTrue() {
		
	}
	
	@Test
	public void whenUserIsNotOwnerOfLeagueWithGivenLeagueIdThenReturnTrue() {
		
	}
	
	@Test
	public void whenUserIsOwnerOfLeagueWithGivenSeasonIdThenReturnTrue() {
		
	}
	
	@Test
	public void whenUserIsNotOwnerOfLeagueWithGivenSeasonIdThenReturnTrue() {
		
	}
	
	
}
