package szczepanski.gerard.betty.service.round;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mockito.internal.util.collections.Sets;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import szczepanski.gerard.betty.domain.league.round.RoundCodeGenerator;
import szczepanski.gerard.betty.domain.league.round.model.Round;
import szczepanski.gerard.betty.domain.league.round.model.RoundFixture;
import szczepanski.gerard.betty.service.common.validator.ValidatorException;
import szczepanski.gerard.betty.service.values.DomainId;

@Test(groups = "unit-tests")
public class RoundFactoryTest {
	
	RoundCodeGenerator roundCodeGenerator;
	RoundFixtureFactory roundFixtureFactory;
	RoundFactory roundFactory;
	
	@BeforeMethod
	public void beforeMethod() {
		roundCodeGenerator = Mockito.mock(RoundCodeGenerator.class);
		roundFixtureFactory = Mockito.mock(RoundFixtureFactory.class);
		
		roundFactory = RoundFactory.builder()
			.roundCodeGenerator(roundCodeGenerator)
			.roundFixtureFactory(roundFixtureFactory)
			.build();
	}
	
	@Test
	public void createRoundSuccess() throws Exception {
		// Arrange
		LocalDateTime eariestStartDate = LocalDateTime.of(2017, 8, 10, 12, 00);
		LocalDateTime latestFinishDate = LocalDateTime.of(2017, 8, 20, 12, 00);
		
		RoundFixture firstRoundFixture = RoundFixture.builder()
				.startDate(eariestStartDate)
				.endDate(LocalDateTime.of(2017, 8, 18, 12, 00))
				.build();
		
		RoundFixture secondRoundFixture = RoundFixture.builder()
				.startDate(LocalDateTime.of(2017, 8, 11, 12, 00))
				.endDate(latestFinishDate)
				.build();
		
		Set<RoundFixture> fixtures = Sets.newSet(firstRoundFixture, secondRoundFixture);
		
		Long rawSeasonId = 14L;
		DomainId seasonId = DomainId.of(rawSeasonId);
		
		RoundCreationDto creationDto = RoundCreationDto.builder()
				.seasonId(seasonId)
				.roundFixturesToCreate(new ArrayList<>())
				.build();
		
		Mockito.when(roundFixtureFactory.createRoundFixtures(Matchers.anyList())).thenReturn(fixtures);
		
		String generatedCode = "RoundCode";
		Mockito.when(roundCodeGenerator.nextCode()).thenReturn(generatedCode);
		
		// Act
		Round createdRound = roundFactory.createRound(creationDto);
		
		// Assert
		Assert.assertNotNull(createdRound);
		Assert.assertNotNull(createdRound.getSeason());
		Assert.assertEquals(createdRound.getSeason().getId(), rawSeasonId);
		Assert.assertEquals(createdRound.getCode(), generatedCode);
		Assert.assertEquals(createdRound.getStartDate(), eariestStartDate);
		Assert.assertEquals(createdRound.getFinishDate(), latestFinishDate);
	}
	
	@Test(expectedExceptions = ValidatorException.class)
	public void whenCreateFixturesThrowsValidationExceptionThenThrowThatExceptionFurther() throws Exception {
		// Arrange
		List<RoundFixtureCreationDto> roundFixtures = new ArrayList<>();
		 
		RoundCreationDto creationDto = RoundCreationDto.builder()
			.roundFixturesToCreate(roundFixtures)
			.build();
		
		Mockito.when(roundFixtureFactory.createRoundFixtures(roundFixtures)).thenThrow(ValidatorException.class);
		
		// Act
		roundFactory.createRound(creationDto);
	}
}
