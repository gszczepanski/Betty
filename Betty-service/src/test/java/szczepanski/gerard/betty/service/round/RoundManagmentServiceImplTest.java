package szczepanski.gerard.betty.service.round;

import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import szczepanski.gerard.betty.common.assertion.ParamAssertionException;
import szczepanski.gerard.betty.domain.league.round.RoundCommandRepository;
import szczepanski.gerard.betty.domain.league.round.model.Round;
import szczepanski.gerard.betty.service.common.validator.ValidatorException;
import szczepanski.gerard.betty.service.values.Code;

@Test(groups = "unit-tests")
public class RoundManagmentServiceImplTest {
	
	RoundCreationValidator roundCreationValidator;
	RoundCommandRepository roundCommandRepository;
	RoundFactory roundFactory;
	RoundManagmentService roundManagmentService;
	
	@BeforeMethod
	public void beforeMethod() {
		roundCreationValidator = Mockito.mock(RoundCreationValidator.class);
		roundCommandRepository = Mockito.mock(RoundCommandRepository.class);
		roundFactory = Mockito.mock(RoundFactory.class);
		
		roundManagmentService = RoundMangmentServiceImpl.builder()
				.roundCreationValidator(roundCreationValidator)
				.roundCommandRepository(roundCommandRepository)
				.roundFactory(roundFactory)
				.build();
	}
	
	@Test
	public void createRoundSuccess() throws Exception {
		// Arrange
		RoundCreationDto creationDto = RoundCreationDto.builder()
				.build();
		
		String expectedCodeValue = "RND-000001";
		Round createdRound = Round.builder()
				.code(expectedCodeValue)
				.build();
		
		Mockito.when(roundFactory.createRound(creationDto)).thenReturn(createdRound);
		
		// Act
		Code codeOfCreatedRound = roundManagmentService.createRound(creationDto);
		
		// Assert
		Assert.assertNotNull(codeOfCreatedRound, "Code of created round can not be null!");
		Assert.assertEquals(codeOfCreatedRound.get(), expectedCodeValue);
		
		Mockito.verify(roundCreationValidator).validate(creationDto);
		Mockito.verify(roundFactory).createRound(creationDto);
		Mockito.verify(roundCommandRepository).create(createdRound);
	}
	
	@Test(expectedExceptions = ParamAssertionException.class)
	public void whenDtoPassedToCreateRoundMethodIsNullThenThrowParamAssertionException() throws Exception { 
		// Arrange
		RoundCreationDto NULL_ROUND_CREATION_DTO = null;
		
		// Act
		roundManagmentService.createRound(NULL_ROUND_CREATION_DTO);
	}
	
	@Test(expectedExceptions = ValidatorException.class)
	public void whenRoundCreationValidatorThrowsValidationExceptionThenThrowException() throws Exception { 
		// Arrange
		RoundCreationDto NOT_VALID_ROUND_CREATION_DTO = RoundCreationDto.builder()
				.build();
		
		Mockito.doThrow(ValidatorException.class).when(roundCreationValidator).validate(NOT_VALID_ROUND_CREATION_DTO);
		
		// Act
		roundManagmentService.createRound(NOT_VALID_ROUND_CREATION_DTO);
	}
	
	
}
