package szczepanski.gerard.betty.service.round;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.mockito.internal.util.collections.Sets;
import org.testng.annotations.Test;

import szczepanski.gerard.betty.integration.service.fixture.IntegrationFixture;
import szczepanski.gerard.betty.integration.service.team.IntegrationTeam;
import szczepanski.gerard.betty.integration.service.value.IntegrationId;
import szczepanski.gerard.betty.service.common.validator.ValidatorException;
import szczepanski.gerard.betty.service.round.RoundFixtureFactory.RoundFixtureFactoryCreationData;

@Test(groups = "unit-tests")
public class RoundFixtureFactoryCreationDataValidatorTest {

	@Test
	public void validateSuccess() throws Exception {
		// Arrange
		RoundFixtureFactoryCreationDataValidator validator = new RoundFixtureFactoryCreationDataValidator();

		IntegrationId firstFixtureId = IntegrationId.of("100000");
		RoundFixtureCreationDto firstFixture = RoundFixtureCreationDto.builder()
				.fixtureIntegrationId(firstFixtureId)
				.build();

		IntegrationId secondFixtureId = IntegrationId.of("20000");
		RoundFixtureCreationDto secondFixture = RoundFixtureCreationDto.builder()
				.fixtureIntegrationId(secondFixtureId)
				.build();

		List<RoundFixtureCreationDto> fixturesToCreate = Arrays.asList(firstFixture, secondFixture);

		IntegrationId firstFixtureHomeTeamId = IntegrationId.of("123");
		IntegrationId firstFixtureAwayTeamId = IntegrationId.of("124");
		IntegrationFixture firstIntegrationFixture = IntegrationFixture.builder()
				.id(firstFixtureId)
				.homeTeamId(firstFixtureHomeTeamId)
				.awayTeamId(firstFixtureAwayTeamId)
				.startDate(LocalDateTime.now().plusDays(2))
				.build();

		IntegrationId secondFixtureHomeTeamId = IntegrationId.of("23");
		IntegrationId secondFixtureAwayTeamId = IntegrationId.of("71");
		IntegrationFixture secondIntegrationFixture = IntegrationFixture.builder()
				.id(secondFixtureId)
				.homeTeamId(secondFixtureHomeTeamId)
				.awayTeamId(secondFixtureAwayTeamId)
				.startDate(LocalDateTime.now().plusDays(2))
				.build();

		Set<IntegrationFixture> fixturesFromIntegration = Sets.newSet(firstIntegrationFixture, secondIntegrationFixture);

		IntegrationTeam firstFixtureHomeTeam = IntegrationTeam.builder()
					.id(firstFixtureHomeTeamId)
					.build();

		IntegrationTeam firstFixtureAwayTeam = IntegrationTeam.builder()
				.id(firstFixtureAwayTeamId)
				.build();

		IntegrationTeam secondFixtureHomeTeam = IntegrationTeam.builder()
				.id(secondFixtureHomeTeamId)
				.build();

		IntegrationTeam secondfirstFixtureAwayTeam = IntegrationTeam.builder()
				.id(secondFixtureAwayTeamId)
				.build();

		Set<IntegrationTeam> teamsFromIntegration = Sets.newSet(firstFixtureHomeTeam, firstFixtureAwayTeam, secondFixtureHomeTeam, secondfirstFixtureAwayTeam);

		Map<IntegrationId, IntegrationFixture> integrationFixturesMap = fixturesFromIntegration.stream()
				.collect(Collectors.toMap(IntegrationFixture::getId, Function.identity()));
		Map<IntegrationId, IntegrationTeam> integrationTeamsMap = teamsFromIntegration.stream()
				.collect(Collectors.toMap(IntegrationTeam::getId, Function.identity()));

		RoundFixtureFactoryCreationData creationData = RoundFixtureFactoryCreationData.builder()
				.creationDtos(fixturesToCreate)
				.integrationFixturesMap(integrationFixturesMap)
				.integrationTeamsMap(integrationTeamsMap)
				.build();

		// Act
		validator.validate(creationData);
	}

	@Test(expectedExceptions = ValidatorException.class)
	public void whenFixturesFromIntegrationAreIncompleteThenThrowException() throws Exception {
		// Arrange
		RoundFixtureFactoryCreationDataValidator validator = new RoundFixtureFactoryCreationDataValidator();

		IntegrationId firstFixtureId = IntegrationId.of("100000");
		RoundFixtureCreationDto firstFixture = RoundFixtureCreationDto.builder()
				.fixtureIntegrationId(firstFixtureId)
				.build();

		IntegrationId secondFixtureId = IntegrationId.of("20000");
		RoundFixtureCreationDto secondFixture = RoundFixtureCreationDto.builder()
				.fixtureIntegrationId(secondFixtureId)
				.build();

		List<RoundFixtureCreationDto> fixturesToCreate = Arrays.asList(firstFixture, secondFixture);

		IntegrationId secondFixtureHomeTeamId = IntegrationId.of("23");
		IntegrationId secondFixtureAwayTeamId = IntegrationId.of("71");
		IntegrationFixture secondIntegrationFixture = IntegrationFixture.builder()
				.id(secondFixtureId)
				.homeTeamId(secondFixtureHomeTeamId)
				.awayTeamId(secondFixtureAwayTeamId)
				.startDate(LocalDateTime.now().plusDays(2))
				.build();

		Set<IntegrationFixture> fixturesFromIntegration = Sets.newSet(secondIntegrationFixture);

		IntegrationTeam secondFixtureHomeTeam = IntegrationTeam.builder()
				.id(secondFixtureHomeTeamId)
				.build();

		IntegrationTeam secondfirstFixtureAwayTeam = IntegrationTeam.builder()
				.id(secondFixtureAwayTeamId)
				.build();

		Set<IntegrationTeam> teamsFromIntegration = Sets.newSet(secondFixtureHomeTeam, secondfirstFixtureAwayTeam);

		Map<IntegrationId, IntegrationFixture> INTEGRATION_FIXTURES_MAP_WITH_MISSING_FIXTURE = fixturesFromIntegration.stream()
				.collect(Collectors.toMap(IntegrationFixture::getId, Function.identity()));
		Map<IntegrationId, IntegrationTeam> integrationTeamsMap = teamsFromIntegration.stream()
				.collect(Collectors.toMap(IntegrationTeam::getId, Function.identity()));

		RoundFixtureFactoryCreationData creationData = RoundFixtureFactoryCreationData.builder()
				.creationDtos(fixturesToCreate)
				.integrationFixturesMap(INTEGRATION_FIXTURES_MAP_WITH_MISSING_FIXTURE)
				.integrationTeamsMap(integrationTeamsMap)
				.build();

		// Act
		validator.validate(creationData);
	}

	@Test(expectedExceptions = ValidatorException.class)
	public void whenTeamsFromIntegrationAreIncompleteThenThrowException() throws Exception {
		// Arrange
		RoundFixtureFactoryCreationDataValidator validator = new RoundFixtureFactoryCreationDataValidator();
		
		IntegrationId firstFixtureId = IntegrationId.of("100000");
		RoundFixtureCreationDto firstFixture = RoundFixtureCreationDto.builder()
				.fixtureIntegrationId(firstFixtureId)
				.build();
		
		IntegrationId secondFixtureId = IntegrationId.of("20000");
		RoundFixtureCreationDto secondFixture = RoundFixtureCreationDto.builder()
				.fixtureIntegrationId(secondFixtureId)
				.build();
		
		List<RoundFixtureCreationDto> fixturesToCreate = Arrays.asList(firstFixture, secondFixture);
		
		IntegrationId firstFixtureHomeTeamId = IntegrationId.of("123");
		IntegrationId firstFixtureAwayTeamId = IntegrationId.of("124");
		IntegrationFixture firstIntegrationFixture = IntegrationFixture.builder()
				.id(firstFixtureId)
				.homeTeamId(firstFixtureHomeTeamId)
				.awayTeamId(firstFixtureAwayTeamId)
				.startDate(LocalDateTime.now().plusDays(2))
				.build();
		
		IntegrationId secondFixtureHomeTeamId = IntegrationId.of("23");
		IntegrationId secondFixtureAwayTeamId = IntegrationId.of("71");
		IntegrationFixture secondIntegrationFixture = IntegrationFixture.builder()
				.id(secondFixtureId)
				.homeTeamId(secondFixtureHomeTeamId)
				.awayTeamId(secondFixtureAwayTeamId)
				.startDate(LocalDateTime.now().plusDays(2))
				.build();
		
		Set<IntegrationFixture> fixturesFromIntegration = Sets.newSet(firstIntegrationFixture, secondIntegrationFixture);
		
		IntegrationTeam firstFixtureHomeTeam = IntegrationTeam.builder()
				.id(firstFixtureHomeTeamId)
				.build();
		
		IntegrationTeam firstFixtureAwayTeam = IntegrationTeam.builder()
				.id(firstFixtureAwayTeamId)
				.build();
		
		IntegrationTeam secondfirstFixtureAwayTeam = IntegrationTeam.builder()
				.id(secondFixtureAwayTeamId)
				.build();
		
		Set<IntegrationTeam> teamsFromIntegration = Sets.newSet(
												firstFixtureHomeTeam, 
												firstFixtureAwayTeam,  
												secondfirstFixtureAwayTeam);
		
		Map<IntegrationId, IntegrationFixture> integrationFixturesMap = fixturesFromIntegration.stream().collect(Collectors.toMap(IntegrationFixture::getId, Function.identity()));
		Map<IntegrationId, IntegrationTeam> TEAMS_MAP_WITH_MISSING_TEAM = teamsFromIntegration.stream().collect(Collectors.toMap(IntegrationTeam::getId, Function.identity()));
		
		RoundFixtureFactoryCreationData creationData = RoundFixtureFactoryCreationData.builder()
				.creationDtos(fixturesToCreate)
				.integrationFixturesMap(integrationFixturesMap)
				.integrationTeamsMap(TEAMS_MAP_WITH_MISSING_TEAM)
				.build();
		
		// Act
		validator.validate(creationData);
	}
	
	@Test(expectedExceptions = ValidatorException.class)
	public void whenAnyFixtureDateIsLessTnahneDayToStartThenThrowException() throws Exception {
		// Arrange
		RoundFixtureFactoryCreationDataValidator validator = new RoundFixtureFactoryCreationDataValidator();

		IntegrationId firstFixtureId = IntegrationId.of("100000");
		RoundFixtureCreationDto firstFixture = RoundFixtureCreationDto.builder()
				.fixtureIntegrationId(firstFixtureId)
				.build();

		IntegrationId secondFixtureId = IntegrationId.of("20000");
		RoundFixtureCreationDto secondFixture = RoundFixtureCreationDto.builder()
				.fixtureIntegrationId(secondFixtureId)
				.build();

		List<RoundFixtureCreationDto> fixturesToCreate = Arrays.asList(firstFixture, secondFixture);

		IntegrationId firstFixtureHomeTeamId = IntegrationId.of("123");
		IntegrationId firstFixtureAwayTeamId = IntegrationId.of("124");
		LocalDateTime LESS_THAN_ONE_DAY_START_DATE = LocalDateTime.now().plusHours(10);
		IntegrationFixture firstIntegrationFixture = IntegrationFixture.builder()
				.id(firstFixtureId)
				.homeTeamId(firstFixtureHomeTeamId)
				.awayTeamId(firstFixtureAwayTeamId)
				.startDate(LESS_THAN_ONE_DAY_START_DATE)
				.build();

		IntegrationId secondFixtureHomeTeamId = IntegrationId.of("23");
		IntegrationId secondFixtureAwayTeamId = IntegrationId.of("71");
		IntegrationFixture secondIntegrationFixture = IntegrationFixture.builder()
				.id(secondFixtureId)
				.homeTeamId(secondFixtureHomeTeamId)
				.awayTeamId(secondFixtureAwayTeamId)
				.startDate(LocalDateTime.now().plusDays(2))
				.build();

		Set<IntegrationFixture> fixturesFromIntegration = Sets.newSet(firstIntegrationFixture, secondIntegrationFixture);

		IntegrationTeam firstFixtureHomeTeam = IntegrationTeam.builder()
					.id(firstFixtureHomeTeamId)
					.build();

		IntegrationTeam firstFixtureAwayTeam = IntegrationTeam.builder()
				.id(firstFixtureAwayTeamId)
				.build();

		IntegrationTeam secondFixtureHomeTeam = IntegrationTeam.builder()
				.id(secondFixtureHomeTeamId)
				.build();

		IntegrationTeam secondfirstFixtureAwayTeam = IntegrationTeam.builder()
				.id(secondFixtureAwayTeamId)
				.build();

		Set<IntegrationTeam> teamsFromIntegration = Sets.newSet(firstFixtureHomeTeam, firstFixtureAwayTeam, secondFixtureHomeTeam, secondfirstFixtureAwayTeam);

		Map<IntegrationId, IntegrationFixture> integrationFixturesMap = fixturesFromIntegration.stream()
				.collect(Collectors.toMap(IntegrationFixture::getId, Function.identity()));
		Map<IntegrationId, IntegrationTeam> integrationTeamsMap = teamsFromIntegration.stream()
				.collect(Collectors.toMap(IntegrationTeam::getId, Function.identity()));

		RoundFixtureFactoryCreationData creationData = RoundFixtureFactoryCreationData.builder()
				.creationDtos(fixturesToCreate)
				.integrationFixturesMap(integrationFixturesMap)
				.integrationTeamsMap(integrationTeamsMap)
				.build();

		// Act
		validator.validate(creationData);
	}

}
