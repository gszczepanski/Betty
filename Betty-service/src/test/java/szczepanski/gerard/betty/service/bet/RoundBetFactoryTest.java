package szczepanski.gerard.betty.service.bet;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.mockito.Mockito;
import org.mockito.internal.util.collections.Sets;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import szczepanski.gerard.betty.domain.league.bet.BetCodeGenerator;
import szczepanski.gerard.betty.domain.league.bet.RoundBetCodeGenerator;
import szczepanski.gerard.betty.domain.league.bet.model.Bet;
import szczepanski.gerard.betty.domain.league.bet.model.RoundBet;
import szczepanski.gerard.betty.domain.league.season.SeasonPlayerCardQueryRepository;
import szczepanski.gerard.betty.domain.league.season.model.SeasonPlayerCard;
import szczepanski.gerard.betty.integration.service.user.CurrentBettyUserService;
import szczepanski.gerard.betty.service.bet.BetRoundDto.BetFixtureDto;
import szczepanski.gerard.betty.service.values.DomainId;

@Test(groups = "unit-tests")
public class RoundBetFactoryTest {
	
	CurrentBettyUserService currentBettyUserService;
	SeasonPlayerCardQueryRepository seasonPlayerCardQueryRepository;
	RoundBetCodeGenerator roundBetCodeGenerator;
	BetCodeGenerator betCodeGenerator;
	RoundBetFactory roundBetFactory;
	
	@BeforeMethod
	public void beforeMethod() {
		currentBettyUserService = Mockito.mock(CurrentBettyUserService.class);
		seasonPlayerCardQueryRepository = Mockito.mock(SeasonPlayerCardQueryRepository.class);
		roundBetCodeGenerator = Mockito.mock(RoundBetCodeGenerator.class);
		betCodeGenerator = Mockito.mock(BetCodeGenerator.class);
		
		roundBetFactory = RoundBetFactory.builder()
				.currentBettyUserService(currentBettyUserService)
				.seasonPlayerCardQueryRepository(seasonPlayerCardQueryRepository)
				.roundBetCodeGenerator(roundBetCodeGenerator)
				.betCodeGenerator(betCodeGenerator)
				.build();
	}
	
	@Test
	public void createRoundBetSuccess() {
		// Arrange
		Long rawFixtureToBetId = 2L;
		DomainId idOfTheFixtureToBet = DomainId.of(rawFixtureToBetId);
		Integer betHomeGoals = 2;
		Integer betAwayGoals = 0;
		
		BetFixtureDto betFixtureDto = BetFixtureDto.builder()
				.roundFixtureId(idOfTheFixtureToBet)
				.homeGoals(betHomeGoals)
				.awayGoals(betAwayGoals)
				.build();
		
		Set<BetFixtureDto> betFixtures = Sets.newSet(betFixtureDto);
		
		Long rawRoundToBetId = 6L;
		DomainId idOfTheRoundToBet = DomainId.of(rawRoundToBetId);
		BetRoundDto betRoundDto = BetRoundDto.builder()
				.roundId(idOfTheRoundToBet)
				.betFixtures(betFixtures)
				.build();
		
		
		String roundBetGeneratedCode = "RBET-457890";
		Mockito.when(roundBetCodeGenerator.nextCode()).thenReturn(roundBetGeneratedCode);
		
		Long rawBettingUserId = 90L;
		DomainId bettingUserId = DomainId.of(rawBettingUserId);
		Mockito.when(currentBettyUserService.getCurrentUserId()).thenReturn(bettingUserId);
		
		Long rawSeasonPlayerCardId = 999L;
		SeasonPlayerCard seasonPlayerCardFromRepo = new SeasonPlayerCard(rawSeasonPlayerCardId);
		Mockito.when(seasonPlayerCardQueryRepository.getPlayerCardByBettyUserId(rawBettingUserId)).thenReturn(seasonPlayerCardFromRepo);
		
		String generatedCodeForFirstBet = "BET-457901";
		List<String> generatedCodesForBets = Arrays.asList(generatedCodeForFirstBet);
		Mockito.when(betCodeGenerator.nextCodes(1)).thenReturn(generatedCodesForBets);
		
		// Act
		RoundBet roundBet = roundBetFactory.createRoundBet(betRoundDto);
		
		// Assert
		Assert.assertNotNull(roundBet);
		
		Assert.assertEquals(roundBet.getCode(), roundBetGeneratedCode);
		Assert.assertEquals(roundBet.getPlayerCard().getId(), rawSeasonPlayerCardId);
		Assert.assertEquals(roundBet.getForRound().getId(), rawRoundToBetId);
		
		Set<Bet> bets = roundBet.getBets();
		
		Assert.assertNotNull(bets);
		Assert.assertFalse(bets.isEmpty());
		
		List<Bet> betsList = bets.stream().collect(Collectors.toList());
		Bet bet = betsList.get(0);
		
		Assert.assertEquals(bet.getFixture().getId(), rawFixtureToBetId);
		Assert.assertEquals(bet.getCode(), generatedCodeForFirstBet);
		Assert.assertEquals(bet.getHomeScoreBet(), betHomeGoals);
		Assert.assertEquals(bet.getAwayScoreBet(), betAwayGoals);
		
		Mockito.verify(roundBetCodeGenerator).nextCode();
		Mockito.verify(currentBettyUserService).getCurrentUserId();
		Mockito.verify(seasonPlayerCardQueryRepository).getPlayerCardByBettyUserId(rawBettingUserId);
		Mockito.verify(betCodeGenerator).nextCodes(1);
	}
	
}
