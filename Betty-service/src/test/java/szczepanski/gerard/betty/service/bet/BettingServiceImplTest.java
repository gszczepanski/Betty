package szczepanski.gerard.betty.service.bet;

import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import szczepanski.gerard.betty.common.assertion.ParamAssertionException;
import szczepanski.gerard.betty.domain.league.bet.RoundBetCommandRepository;
import szczepanski.gerard.betty.domain.league.bet.model.RoundBet;
import szczepanski.gerard.betty.service.values.Code;

@Test(groups = "unit-tests")
public class BettingServiceImplTest {
	
	BetRoundValidator betRoundValidator;
	RoundBetFactory roundBetFactory;
	RoundBetCommandRepository betCommandRepository;
	BettingService bettingService;
	
	@BeforeMethod
	public void beforeMethod() {
		betRoundValidator = Mockito.mock(BetRoundValidator.class);
		roundBetFactory = Mockito.mock(RoundBetFactory.class);
		betCommandRepository = Mockito.mock(RoundBetCommandRepository.class);
		
		bettingService = BettingServiceImpl.builder()
				.betRoundValidator(betRoundValidator)
				.betFactory(roundBetFactory)
				.betCommandRepository(betCommandRepository)
				.build();
	}
	
	@Test
	public void betRoundSuccess() throws Exception {
		// Arrange
		String createdBetCode = "RBET-000001";
		
		BetRoundDto betRoundDto = BetRoundDto.builder().build();
		RoundBet betCreatedByFactory = RoundBet.builder()
				.code(createdBetCode)
				.build();
		
		Mockito.when(roundBetFactory.createRoundBet(betRoundDto)).thenReturn(betCreatedByFactory);
		
		// Act
		Code code = bettingService.betRound(betRoundDto);
		
		// Assert
		Assert.assertNotNull(code);
		Assert.assertEquals(code.get(), createdBetCode);
		
		Mockito.verify(betRoundValidator).validate(betRoundDto);
		Mockito.verify(roundBetFactory).createRoundBet(betRoundDto);
		Mockito.verify(betCommandRepository).create(betCreatedByFactory);
	}
	
	@Test(expectedExceptions = ParamAssertionException.class)
	public void whenDtoPassedToCreateBetMethodIsNullThenThrowParamAssertionException() throws Exception { 
		// Arrange
		BetRoundDto NULL_BET_CREATION_DTO = null;
		
		// Act
		bettingService.betRound(NULL_BET_CREATION_DTO);
	}
	
}
