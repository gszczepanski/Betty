package szczepanski.gerard.betty.service.values;

import org.testng.Assert;
import org.testng.annotations.Test;

import szczepanski.gerard.betty.common.assertion.ParamAssertionException;

@Test(groups = "unit-tests")
public class DomainIdTest {
	
	@Test(expectedExceptions = ParamAssertionException.class)
	public void whenIdIsNullThenThrowExcpetion() {
		// Arrange
		Long NULL_ID = null;	 
		
		// Act
		DomainId id = DomainId.of(NULL_ID);
	}
	
	@Test(expectedExceptions = ParamAssertionException.class)
	public void whenIdIsNegativeThenThrowExcpetion() {
		// Arrange
		Long NEGATIVE_ID = -30L;	 
		
		// Act
		DomainId id = DomainId.of(NEGATIVE_ID);
	}
	
	@Test
	public void createDomainId() {
		// Arrange
		Long validId = 1992L;
		
		// Act
		DomainId id = DomainId.of(validId);
		
		// Assert
		Assert.assertNotNull(id);
		Assert.assertEquals(id.get(), validId);
	}
	
}
