package szczepanski.gerard.betty.service.round;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import szczepanski.gerard.betty.domain.league.round.RoundQueryRepository;
import szczepanski.gerard.betty.domain.league.round.model.RoundFixtureType;
import szczepanski.gerard.betty.domain.league.season.SeasonQueryRepository;
import szczepanski.gerard.betty.integration.service.value.IntegrationId;
import szczepanski.gerard.betty.service.common.validator.ValidatorException;
import szczepanski.gerard.betty.service.league.LeagueOwnershipChecker;
import szczepanski.gerard.betty.service.values.DomainId;

@Test(groups = "unit-tests")
public class RoundCreationValidatorTest {
	
	LeagueOwnershipChecker leagueOwnershipChecker;
	RoundQueryRepository roundQueryRepository;
	SeasonQueryRepository seasonQueryRepository;
	RoundCreationValidator roundCreationValidator;
	
	@BeforeMethod
	public void beforeMethod() {
		leagueOwnershipChecker = Mockito.mock(LeagueOwnershipChecker.class);
		roundQueryRepository = Mockito.mock(RoundQueryRepository.class);
		seasonQueryRepository = Mockito.mock(SeasonQueryRepository.class);
		roundCreationValidator = RoundCreationValidator.builder()
				.leagueOwnershipChecker(leagueOwnershipChecker)
				.roundQueryRepository(roundQueryRepository)
				.seasonQueryRepository(seasonQueryRepository)
				.build();
	}
	
	@Test
	public void validateValidDto() throws Exception {
		// Arrange
		IntegrationId firstFixtureId = IntegrationId.of("10");
		String firstFixtureType = RoundFixtureType.NORMAL.toString();
		RoundFixtureCreationDto firstFixture = RoundFixtureCreationDto.builder()
				.fixtureIntegrationId(firstFixtureId)
				.fixtureType(firstFixtureType)
				.build();
		
		Long rawSeasonId = 123L;
		DomainId seasonId = DomainId.of(rawSeasonId);
		List<RoundFixtureCreationDto> fixtures = Arrays.asList(firstFixture);
		
		RoundCreationDto validDto = RoundCreationDto.builder()
				.seasonId(seasonId)
				.roundFixturesToCreate(fixtures)
				.build();
		
		Mockito.when(leagueOwnershipChecker.isOwnerOfSeasonAction(seasonId)).thenReturn(true);
		Mockito.when(roundQueryRepository.isAllRoundsAreFinishedForSeason(rawSeasonId)).thenReturn(true);
		Mockito.when(seasonQueryRepository.canAddNewRound(rawSeasonId)).thenReturn(true);
		
		// Act
		roundCreationValidator.validate(validDto);
		
		// Assert
		Mockito.verify(leagueOwnershipChecker).isOwnerOfSeasonAction(seasonId);
		Mockito.verify(roundQueryRepository).isAllRoundsAreFinishedForSeason(rawSeasonId);
		Mockito.verify(seasonQueryRepository).canAddNewRound(rawSeasonId);
	}
	
	@Test(expectedExceptions = ValidatorException.class)
	public void whenSeasonIdIsNullThenThrowValidatorException() throws Exception {
		// Arrange
		DomainId NULL_SEASON_ID = null;
		RoundCreationDto dto = RoundCreationDto.builder()
				.seasonId(NULL_SEASON_ID)
				.build();
		
		// Act
		roundCreationValidator.validate(dto);
	}
	
	@Test(expectedExceptions = ValidatorException.class)
	public void whenUserThatWantToCreateRoundIsNotOwnerOfLeagueThenThrowValidatorException() throws Exception {
		// Arrange
		DomainId seasonId = DomainId.of(123L);
		RoundCreationDto dto = RoundCreationDto.builder()
				.seasonId(seasonId)
				.build();
		
		Mockito.when(leagueOwnershipChecker.isOwnerOfSeasonAction(seasonId)).thenReturn(false);
		
		// Act
		roundCreationValidator.validate(dto);
	}
	
	@Test(expectedExceptions = ValidatorException.class)
	public void whenAllRoundsInSeasonAreNotFinishedThenThrowValidatorException() throws Exception {
		// Arrange
		Long rawSeasonId = 123L;
		DomainId seasonId = DomainId.of(rawSeasonId);
		RoundCreationDto dto = RoundCreationDto.builder()
				.seasonId(seasonId)
				.build();
		
		Mockito.when(leagueOwnershipChecker.isOwnerOfSeasonAction(seasonId)).thenReturn(true);
		Mockito.when(roundQueryRepository.isAllRoundsAreFinishedForSeason(rawSeasonId)).thenReturn(false);
		
		// Act
		roundCreationValidator.validate(dto);
	}
	
	@Test(expectedExceptions = ValidatorException.class)
	public void whenSeasonIsFinishedThenThrowValidatorException() throws Exception {
		// Arrange
		Long rawSeasonId = 123L;
		DomainId seasonId = DomainId.of(rawSeasonId);
		RoundCreationDto dto = RoundCreationDto.builder()
				.seasonId(seasonId)
				.build();
		
		Mockito.when(leagueOwnershipChecker.isOwnerOfSeasonAction(seasonId)).thenReturn(true);
		Mockito.when(roundQueryRepository.isAllRoundsAreFinishedForSeason(rawSeasonId)).thenReturn(true);
		Mockito.when(seasonQueryRepository.canAddNewRound(rawSeasonId)).thenReturn(false);
		
		// Act
		roundCreationValidator.validate(dto);
	}
	
	@Test(expectedExceptions = ValidatorException.class)
	public void whenCollectionOfFixturesIsNullThenThrowValidatorException() throws Exception {
		// Arrange
		Long rawSeasonId = 123L;
		DomainId seasonId = DomainId.of(rawSeasonId);
		
		List<RoundFixtureCreationDto> NULL_FIXTURES = null;
		RoundCreationDto dto = RoundCreationDto.builder()
				.seasonId(seasonId)
				.roundFixturesToCreate(NULL_FIXTURES)
				.build();
		
		Mockito.when(leagueOwnershipChecker.isOwnerOfSeasonAction(seasonId)).thenReturn(true);
		Mockito.when(roundQueryRepository.isAllRoundsAreFinishedForSeason(rawSeasonId)).thenReturn(true);
		Mockito.when(seasonQueryRepository.canAddNewRound(rawSeasonId)).thenReturn(true);
		
		// Act
		roundCreationValidator.validate(dto);
	}
	
	@Test(expectedExceptions = ValidatorException.class)
	public void whenCollectionOfFixturesIsEmptyThenThrowValidatorException() throws Exception {
		// Arrange
		Long rawSeasonId = 123L;
		DomainId seasonId = DomainId.of(rawSeasonId);
		
		List<RoundFixtureCreationDto> EMPTY_FIXTURES = new ArrayList<>();
		RoundCreationDto dto = RoundCreationDto.builder()
				.seasonId(seasonId)
				.roundFixturesToCreate(EMPTY_FIXTURES)
				.build();
		
		Mockito.when(leagueOwnershipChecker.isOwnerOfSeasonAction(seasonId)).thenReturn(true);
		Mockito.when(roundQueryRepository.isAllRoundsAreFinishedForSeason(rawSeasonId)).thenReturn(true);
		Mockito.when(seasonQueryRepository.canAddNewRound(rawSeasonId)).thenReturn(true);
		
		// Act
		roundCreationValidator.validate(dto);
	}
	
	@Test(expectedExceptions = ValidatorException.class)
	public void whenIntegrationIdIsMissingForAnyFixtureThenThrowException() throws Exception {
		// Arrange
		IntegrationId NULL_FIXTURE_ID = null;
		String firstFixtureType = RoundFixtureType.NORMAL.toString();
		RoundFixtureCreationDto firstFixture = RoundFixtureCreationDto.builder()
				.fixtureIntegrationId(NULL_FIXTURE_ID)
				.fixtureType(firstFixtureType)
				.build();
		
		Long rawSeasonId = 123L;
		DomainId seasonId = DomainId.of(rawSeasonId);
		List<RoundFixtureCreationDto> fixtures = Arrays.asList(firstFixture);
		
		RoundCreationDto validDto = RoundCreationDto.builder()
				.seasonId(seasonId)
				.roundFixturesToCreate(fixtures)
				.build();
		
		Mockito.when(leagueOwnershipChecker.isOwnerOfSeasonAction(seasonId)).thenReturn(true);
		Mockito.when(roundQueryRepository.isAllRoundsAreFinishedForSeason(rawSeasonId)).thenReturn(true);
		Mockito.when(seasonQueryRepository.canAddNewRound(rawSeasonId)).thenReturn(true);
		
		// Act
		roundCreationValidator.validate(validDto);
	}
	
	@Test(expectedExceptions = ValidatorException.class)
	public void whenFixturesIntegrationIdsAreDuplicatedThenThrowException() throws Exception {
		// Arrange
		IntegrationId DUPLICTED_FIXTURE_ID = IntegrationId.of("456");
		String firstFixtureType = RoundFixtureType.NORMAL.toString();
		RoundFixtureCreationDto firstFixture = RoundFixtureCreationDto.builder()
				.fixtureIntegrationId(DUPLICTED_FIXTURE_ID)
				.fixtureType(firstFixtureType)
				.build();
		
		String secondFixtureType = RoundFixtureType.NORMAL.toString();
		RoundFixtureCreationDto secondFixture = RoundFixtureCreationDto.builder()
				.fixtureIntegrationId(DUPLICTED_FIXTURE_ID)
				.fixtureType(secondFixtureType)
				.build();
		
		Long rawSeasonId = 123L;
		DomainId seasonId = DomainId.of(rawSeasonId);
		List<RoundFixtureCreationDto> fixtures = Arrays.asList(firstFixture, secondFixture);
		
		RoundCreationDto validDto = RoundCreationDto.builder()
				.seasonId(seasonId)
				.roundFixturesToCreate(fixtures)
				.build();
		
		Mockito.when(leagueOwnershipChecker.isOwnerOfSeasonAction(seasonId)).thenReturn(true);
		Mockito.when(roundQueryRepository.isAllRoundsAreFinishedForSeason(rawSeasonId)).thenReturn(true);
		Mockito.when(seasonQueryRepository.canAddNewRound(rawSeasonId)).thenReturn(true);
		
		// Act
		roundCreationValidator.validate(validDto);
	}
	
	@Test(expectedExceptions = ValidatorException.class)
	public void whenFixtureTypeIsMissingForAnyFixtureThenThrowException() throws Exception {
		// Arrange
		IntegrationId firstFixtureId = IntegrationId.of("10");
		String NULL_FIXTURE_TYPE = null;
		RoundFixtureCreationDto firstFixture = RoundFixtureCreationDto.builder()
				.fixtureIntegrationId(firstFixtureId)
				.fixtureType(NULL_FIXTURE_TYPE)
				.build();
		
		Long rawSeasonId = 123L;
		DomainId seasonId = DomainId.of(rawSeasonId);
		List<RoundFixtureCreationDto> fixtures = Arrays.asList(firstFixture);
		
		RoundCreationDto validDto = RoundCreationDto.builder()
				.seasonId(seasonId)
				.roundFixturesToCreate(fixtures)
				.build();
		
		Mockito.when(leagueOwnershipChecker.isOwnerOfSeasonAction(seasonId)).thenReturn(true);
		Mockito.when(roundQueryRepository.isAllRoundsAreFinishedForSeason(rawSeasonId)).thenReturn(true);
		Mockito.when(seasonQueryRepository.canAddNewRound(rawSeasonId)).thenReturn(true);
		
		// Act
		roundCreationValidator.validate(validDto);
	}
	
	@Test(expectedExceptions = ValidatorException.class)
	public void whenFixtureTypeCannotBeMappedToDomainEnumThenThrowException() throws Exception {
		// Arrange
		IntegrationId firstFixtureId = IntegrationId.of("10");
		String FAKE_FIXTURE_TYPE = "notInEnum";
		RoundFixtureCreationDto firstFixture = RoundFixtureCreationDto.builder()
				.fixtureIntegrationId(firstFixtureId)
				.fixtureType(FAKE_FIXTURE_TYPE)
				.build();
		
		Long rawSeasonId = 123L;
		DomainId seasonId = DomainId.of(rawSeasonId);
		List<RoundFixtureCreationDto> fixtures = Arrays.asList(firstFixture);
		
		RoundCreationDto validDto = RoundCreationDto.builder()
				.seasonId(seasonId)
				.roundFixturesToCreate(fixtures)
				.build();
		
		Mockito.when(leagueOwnershipChecker.isOwnerOfSeasonAction(seasonId)).thenReturn(true);
		Mockito.when(roundQueryRepository.isAllRoundsAreFinishedForSeason(rawSeasonId)).thenReturn(true);
		Mockito.when(seasonQueryRepository.canAddNewRound(rawSeasonId)).thenReturn(true);
		
		// Act
		roundCreationValidator.validate(validDto);
	}

}
