package szczepanski.gerard.betty.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import szczepanski.gerard.betty.service.config.CommonBettyServiceConfiguration;
import szczepanski.gerard.betty.service.round.RoundServiceSpringConfiguration;
import szczepanski.gerard.betty.service.scheduler.round.RoundServiceSchedulerSpringConfiguration;
import szczepanski.gerard.betty.web.home.HomePageController;

@Configuration
@Import(value = {
		CommonBettyServiceConfiguration.class,
		RoundServiceSpringConfiguration.class,
		RoundServiceSchedulerSpringConfiguration.class
	})
public class BettyWebSpringConfiguration {

	@Bean
	public HomePageController homePageController() {
		return new HomePageController();
	}
}
