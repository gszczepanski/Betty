package szczepanski.gerard.betty.web.home;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import szczepanski.gerard.betty.common.logger.BettyLogger;
import szczepanski.gerard.betty.common.logger.BettyLoggerFactory;

@Controller
public class HomePageController {
	private static final BettyLogger LOG = BettyLoggerFactory.createForName(HomePageController.class.getName());
	
	private static final String HOME_PAGE = "homePage/homePage";
	
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String showHomePage(Model model) {
		LOG.debug("showHomePage");
		
		return HOME_PAGE;
	}
	
}
