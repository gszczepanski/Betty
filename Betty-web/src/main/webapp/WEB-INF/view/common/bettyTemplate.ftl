<#macro bettyTemplate>
	<html>
		<head>
			<link rel="stylesheet" href="../css/bootstrap.min.css">
		</head>
		<body>
			<div class="container">
				<div class="panel-heading">
					<div class="jumbotron">
						<#nested>
					</div>
				</div>
			</div>
		</body>
	</html>
</#macro>