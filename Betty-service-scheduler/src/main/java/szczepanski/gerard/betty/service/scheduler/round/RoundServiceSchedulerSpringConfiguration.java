package szczepanski.gerard.betty.service.scheduler.round;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;

import szczepanski.gerard.betty.service.round.FinishedRoundCalculator;
import szczepanski.gerard.betty.service.round.FinishedRoundFinder;
import szczepanski.gerard.betty.service.round.FinishedRoundResultFetcher;
import szczepanski.gerard.betty.service.round.RoundServiceSpringConfiguration;

@Configuration
@Import(value = {RoundServiceSpringConfiguration.class})
@EnableScheduling
public class RoundServiceSchedulerSpringConfiguration {
	
	@Autowired
	private FinishedRoundFinder finishedRoundFinder;
	
	@Autowired
	private FinishedRoundResultFetcher finishedRoundResultFetcher;
	
	@Autowired
	private FinishedRoundCalculator finishedRoundCalculator;
	
	@Bean
	public FinishedRoundScheduler finishedRoundScheduler() {
		return FinishedRoundScheduler.builder()
				.finishedRoundFinder(finishedRoundFinder)
				.finishedRoundResultFetcher(finishedRoundResultFetcher)
				.finishedRoundCalculator(finishedRoundCalculator)
				.build();
	}
	
	
}
