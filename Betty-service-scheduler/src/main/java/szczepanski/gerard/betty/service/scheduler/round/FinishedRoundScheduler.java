package szczepanski.gerard.betty.service.scheduler.round;

import org.springframework.scheduling.annotation.Scheduled;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.betty.common.logger.BettyLogger;
import szczepanski.gerard.betty.common.logger.BettyLoggerFactory;
import szczepanski.gerard.betty.service.round.FinishedRoundCalculator;
import szczepanski.gerard.betty.service.round.FinishedRoundFinder;
import szczepanski.gerard.betty.service.round.FinishedRoundResultFetcher;

@Builder
@RequiredArgsConstructor
class FinishedRoundScheduler {
	private static final BettyLogger LOG = BettyLoggerFactory.createForName(FinishedRoundScheduler.class.getName());
	
	private final FinishedRoundFinder finishedRoundFinder;
	private final FinishedRoundResultFetcher finishedRoundResultFetcher;
	private final FinishedRoundCalculator finishedRoundCalculator;
	
	@Scheduled(cron = "0 10,40 0 * * *")
	public void findJustFinishedRounds() {
		LOG.debug("Triggering scheduled finding just finished rounds");
		finishedRoundFinder.findThenPublishFinishedRounds();
	}
	
	@Scheduled(cron = "0 20,50 0 * * *")
	public void fetchFinishedRoundsResults() {
		LOG.debug("Triggering scheduled fetching finished rounds results");
		finishedRoundResultFetcher.fetchResultsForRound();
	}
	
	@Scheduled(cron = "0 0,30 0 * * *")
	public void caculateClosedRounds() {
		LOG.debug("Triggerind scheduled calcuate closed rounds");
		finishedRoundCalculator.summarizeFinishedRounds();
	}
	
}
