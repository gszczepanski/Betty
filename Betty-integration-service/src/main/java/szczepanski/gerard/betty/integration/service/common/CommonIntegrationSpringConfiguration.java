package szczepanski.gerard.betty.integration.service.common;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CommonIntegrationSpringConfiguration {
	
	@Bean
	public IntegrationRestClient integrationRestClient() {
		return SimpleIntegrationRestClient.createForHost(IntegrationValues.REST_API_HOST);
	}
	
}
