package szczepanski.gerard.betty.integration.service.common;

import szczepanski.gerard.betty.common.exception.BettyException;

public class BettyIntegrationRestException extends BettyException {
	private static final long serialVersionUID = 62897120755130556L;

	public BettyIntegrationRestException() {
		super();
	}

	public BettyIntegrationRestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public BettyIntegrationRestException(String message, Throwable cause) {
		super(message, cause);
	}

	public BettyIntegrationRestException(String message) {
		super(message);
	}

	public BettyIntegrationRestException(Throwable cause) {
		super(cause);
	}
	
}
