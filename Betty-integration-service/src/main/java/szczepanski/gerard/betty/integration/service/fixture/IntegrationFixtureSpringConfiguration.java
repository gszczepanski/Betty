package szczepanski.gerard.betty.integration.service.fixture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import szczepanski.gerard.betty.integration.service.common.CommonIntegrationSpringConfiguration;
import szczepanski.gerard.betty.integration.service.common.IntegrationRestClient;

@Configuration
@Import(value = CommonIntegrationSpringConfiguration.class)
public class IntegrationFixtureSpringConfiguration {
	
	@Autowired
	private IntegrationRestClient integrationRestClient;
	
	@Bean
	public FixtureIntegrationService fixtureIntegrationService() {
		return new FixtureIntegrationServiceImpl(integrationRestClient, fixtureJsonResponseMapper());
	}
	
	@Bean
	public FixtureJsonResponseMapper fixtureJsonResponseMapper() {
		return new FixtureJsonResponseMapper();
	}
	
}
