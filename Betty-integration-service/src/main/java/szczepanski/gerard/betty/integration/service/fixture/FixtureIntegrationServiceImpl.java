package szczepanski.gerard.betty.integration.service.fixture;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.betty.common.assertion.ParamAssertion;
import szczepanski.gerard.betty.common.logger.BettyLogger;
import szczepanski.gerard.betty.common.logger.BettyLoggerFactory;
import szczepanski.gerard.betty.integration.service.common.BettyIntegrationMappingException;
import szczepanski.gerard.betty.integration.service.common.BettyIntegrationRestException;
import szczepanski.gerard.betty.integration.service.common.IntegrationRestClient;
import szczepanski.gerard.betty.integration.service.value.IntegrationId;

@RequiredArgsConstructor
class FixtureIntegrationServiceImpl implements FixtureIntegrationService {
	
	private static final BettyLogger LOG = BettyLoggerFactory.createForName(FixtureIntegrationService.class.getName());
	private static final String FIND_FIXTURE_PATH = "/fixtures/%s";
	
	private final IntegrationRestClient restClient;
	private final FixtureJsonResponseMapper fixtureMapper;
	
	@Override
	public Optional<IntegrationFixture> findOne(IntegrationId id) {
		LOG.debug("Find fixture for id %s", id);
		String path = String.format(FIND_FIXTURE_PATH, id);
		try {
			String jsonResponse = restClient.fireGetRequest(path);
			IntegrationFixture fixture = fixtureMapper.map(jsonResponse);
			return Optional.of(fixture);
		} catch (BettyIntegrationRestException e) {
			LOG.error("Unable to find fixture with id: %s", e, id);
		} catch (BettyIntegrationMappingException e) {
			LOG.error("Mapping exception occured for fixture with id: %s", e, id);
		}
		return Optional.empty();
	}

	@Override
	public Set<IntegrationFixture> findMany(List<IntegrationId> ids) {
		ParamAssertion.collectionNotEmpty(ids);
		LOG.debug("Find fixtures with ids %s", ids);
		
		Set<Optional<IntegrationFixture>> fixtures = ids.parallelStream().map(this::findOne).collect(Collectors.toSet());
		Set<IntegrationFixture> foundFixtures = fixtures.stream().filter(optional -> optional.isPresent()).map(Optional::get).collect(Collectors.toSet());
		
		Set<IntegrationId> foundFixturesIds = foundFixtures.stream().map(IntegrationFixture::getId).collect(Collectors.toSet());
		LOG.debug("Fixtures found: %s", foundFixturesIds);
		
		return foundFixtures;
	}

}
