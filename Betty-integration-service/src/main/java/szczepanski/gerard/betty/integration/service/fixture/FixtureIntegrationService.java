package szczepanski.gerard.betty.integration.service.fixture;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import szczepanski.gerard.betty.integration.service.value.IntegrationId;

public interface FixtureIntegrationService {
	
	Optional<IntegrationFixture> findOne(IntegrationId id);
	
	Set<IntegrationFixture> findMany(List<IntegrationId> ids);
	
}
