package szczepanski.gerard.betty.integration.service.team;

import lombok.Builder;
import lombok.Getter;
import szczepanski.gerard.betty.integration.service.value.IntegrationId;

@Getter
@Builder
public class IntegrationTeam {
	
	private final IntegrationId id;
	private final String name;
	private final String imageURL;
	
}
