package szczepanski.gerard.betty.integration.service.team;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.betty.common.assertion.ParamAssertion;
import szczepanski.gerard.betty.integration.service.common.JsonResponseMapper;
import szczepanski.gerard.betty.integration.service.value.IntegrationId;

@RequiredArgsConstructor
class TeamJsonResponseMapper extends JsonResponseMapper<IntegrationTeam>{
	
	private static final String NAME_PROPERTY = "name";
	private static final String IMAGE_URL_PROPERTY = "crestUrl";
	
	@Override
	protected IntegrationTeam doMapping(String json) {
		ParamAssertion.isNotBlank(json);
		
		IntegrationId id = IntegrationId.of(getId(json));
		String name = getJsonValue(NAME_PROPERTY, json);
		String imageURL = getJsonValue(IMAGE_URL_PROPERTY, json);
		
		return IntegrationTeam
					.builder()
					.id(id)
					.name(name)
					.imageURL(imageURL)
					.build();
	}
	
}
