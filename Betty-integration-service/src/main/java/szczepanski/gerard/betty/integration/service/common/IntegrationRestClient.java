package szczepanski.gerard.betty.integration.service.common;

@FunctionalInterface
public interface IntegrationRestClient {
	
	/**
	 * Fire get request for given path.
	 * For example if path is "/team/1", then
	 * implementation fire GET request for given impl host and pat for example:
	 * http://localhost:8080/team/1.
	 * 
	 * This method returns given response JSON.
	 */
	String fireGetRequest(String path) throws BettyIntegrationRestException;
	
}
