package szczepanski.gerard.betty.integration.service.team;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import szczepanski.gerard.betty.integration.service.value.IntegrationId;

public interface TeamIntegrationService {
	
	Set<IntegrationTeam> findByName(String name);
	
	Optional<IntegrationTeam> findOne(IntegrationId id);
	
	Set<IntegrationTeam> findMany(List<IntegrationId> ids);
	
}
