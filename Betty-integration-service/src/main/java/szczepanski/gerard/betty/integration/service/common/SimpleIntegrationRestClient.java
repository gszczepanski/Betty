package szczepanski.gerard.betty.integration.service.common;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import szczepanski.gerard.betty.common.assertion.ParamAssertion;
import szczepanski.gerard.betty.common.logger.BettyLogger;
import szczepanski.gerard.betty.common.logger.BettyLoggerFactory;

public class SimpleIntegrationRestClient implements IntegrationRestClient {
	private static final BettyLogger LOG = BettyLoggerFactory.createForName(IntegrationRestClient.class.getName());
	
	private final String restApiHost;
	
	public static SimpleIntegrationRestClient createForHost(String restApiHost) {
		ParamAssertion.isNotBlank(restApiHost);
		return new SimpleIntegrationRestClient(restApiHost);
	}
	
	private SimpleIntegrationRestClient(String restApiHost) {
		this.restApiHost = restApiHost;
	}
	
	public String fireGetRequest(String path) throws BettyIntegrationRestException {
		String requestUrl = restApiHost + path;
		LOG.debug("OUTCOMING GET REQUEST: %s", requestUrl);
		
		try {
			HttpResponse<JsonNode> response = 
						Unirest
							.get(requestUrl)
							.asJson();
			
			logHttpResponse(response);
			handleResponseStatus(response);
			
			return response.getBody().toString();
		} catch (UnirestException e) {
			throw new BettyIntegrationRestException(e);
		}
	}
	
	private void logHttpResponse(HttpResponse<JsonNode> response) {
		String status = String.valueOf(response.getStatus());
		String statusText = response.getStatusText();
		String headers = response.getHeaders().toString();
		String body = response.getBody().toString();
		
		LOG.debug("INCOMING RESPONSE\nStatus: %s %s\nHeaders: %s\nBody: %s", status, statusText, headers, body);
	}
	
	private void handleResponseStatus(HttpResponse<JsonNode> response) throws BettyIntegrationRestException {
		Integer responseStatus = response.getStatus();
		
		if (responseStatus >= 400) {
			throw new BettyIntegrationRestException(String.format("Unhandled Response Status: %s", responseStatus));
		}
	}
	
}
