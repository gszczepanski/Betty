package szczepanski.gerard.betty.integration.service.common;

import szczepanski.gerard.betty.common.exception.BettyException;

public class BettyIntegrationMappingException extends BettyException {
	private static final long serialVersionUID = 3217609753125045888L;

	public BettyIntegrationMappingException() {
		super();
	}

	public BettyIntegrationMappingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public BettyIntegrationMappingException(String message, Throwable cause) {
		super(message, cause);
	}

	public BettyIntegrationMappingException(String message) {
		super(message);
	}

	public BettyIntegrationMappingException(Throwable cause) {
		super(cause);
	}
	
}
