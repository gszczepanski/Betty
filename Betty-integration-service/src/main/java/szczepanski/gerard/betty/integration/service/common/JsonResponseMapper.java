package szczepanski.gerard.betty.integration.service.common;

import org.json.JSONObject;

/**
 * Based on template method pattern, this is abstract class for Mappers, that map response JSON String into
 * Integration objects. 
 * 
 * Derived classes must extend doMapping method, which actually map JSOn to object.
 * If any exception occured, then it is handled in map method and BettyIntegrationMappingException
 * is being thrown.
 * 
 * @author Gerard Szczepanski
 */
public abstract class JsonResponseMapper<T> {
	
	protected static final String LINKS_PROPERTY = "_links";
	protected static final String SELF_PROPERTY = "self";
	protected static final String HREF = "href";
	
	public final T map(String json) throws BettyIntegrationMappingException {
		try {
			return doMapping(json);
		} catch (Throwable e) {
			throw new BettyIntegrationMappingException(e);
		}
	}
	
	protected abstract T doMapping(String json);
	
	protected String getJsonValue(String propertyName, String json) {
		 JSONObject jsonObject = new JSONObject(json);
		 return jsonObject.getString(propertyName);
	}
	
	protected JSONObject getJsonObject(String propertyName, String json) {
		 JSONObject jsonObject = new JSONObject(json);
		 return jsonObject.getJSONObject(propertyName);
	}
	
	protected String getHrefIdFromJson(String json) {
		String href = getJsonValue(HREF, json);
		return getIdFromResourceHref(href);
	}
	
	protected String getId(String json) {
		 JSONObject links = getJsonObject(LINKS_PROPERTY, json);
		 JSONObject self = links.getJSONObject(SELF_PROPERTY);
		 return getHrefIdFromJsonObject(self);
	}
	
	protected String getHrefIdFromJsonObject(JSONObject jsonObject) {
		String href = jsonObject.getString(HREF);
		return getIdFromResourceHref(href);
	}
	
	protected String getIdFromResourceHref(String href) {
		String[] splitedHref = href.split("/");
		return splitedHref[splitedHref.length - 1];
	}
	
}
