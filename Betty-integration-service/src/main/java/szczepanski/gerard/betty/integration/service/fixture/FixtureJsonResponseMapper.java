package szczepanski.gerard.betty.integration.service.fixture;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import org.json.JSONObject;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.betty.common.assertion.ParamAssertion;
import szczepanski.gerard.betty.integration.service.common.JsonResponseMapper;
import szczepanski.gerard.betty.integration.service.fixture.IntegrationFixture.FixtureStatus;
import szczepanski.gerard.betty.integration.service.fixture.IntegrationFixture.Result;
import szczepanski.gerard.betty.integration.service.value.IntegrationId;

@RequiredArgsConstructor
class FixtureJsonResponseMapper extends JsonResponseMapper<IntegrationFixture> {
	
	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	private static final String FIXTURE = "fixture";
	private static final String HOME_TEAM = "homeTeam";
	private static final String AWAY_TEAM = "awayTeam";
	private static final String DATE = "date";
	private static final String STATUS = "status";
	private static final String RESULT = "result";
	private static final String GOALS_HOME_TEAM = "goalsHomeTeam";
	private static final String GOALS_AWAY_TEAM = "goalsAwayTeam";
	
	@Override
	protected IntegrationFixture doMapping(String json) {
		ParamAssertion.isNotBlank(json);
		
		JSONObject fixtureJsonObj = getJsonObject(FIXTURE, json);
		String fixtureJson = fixtureJsonObj.toString();
		
		IntegrationId id = IntegrationId.of(getId(fixtureJson));
		IntegrationId homeTeamId = IntegrationId.of(getTeamIdFromFixture(HOME_TEAM, fixtureJson));
		IntegrationId awayTeamId = IntegrationId.of(getTeamIdFromFixture(AWAY_TEAM, fixtureJson));
		
		LocalDateTime startDate = getDate(fixtureJson);
				
		String statusAsString = getJsonValue(STATUS, fixtureJson);
		FixtureStatus status = FixtureStatus.valueOf(statusAsString);
		
		Result result = getResult(fixtureJson);
		Optional<Result> optionalResult = Optional.ofNullable(result);
		
		return IntegrationFixture
					.builder()
					.id(id)
					.homeTeamId(homeTeamId)
					.awayTeamId(awayTeamId)
					.startDate(startDate)
					.status(status)
					.result(optionalResult)
					.build();
	}
	
	private String getTeamIdFromFixture(String propertyName, String fixtureJson) {
		JSONObject links = getJsonObject(LINKS_PROPERTY, fixtureJson);
		JSONObject team = links.getJSONObject(propertyName);
		String href = team.getString(HREF);
		return getIdFromResourceHref(href);
	}
	
	private LocalDateTime getDate(String fixtureJson) {
		String startDateAsString = getJsonValue(DATE, fixtureJson);
		String escapedDateString = startDateAsString.replace('T', ' ');
		escapedDateString = escapedDateString.substring(0, escapedDateString.length() - 1);
		return LocalDateTime.parse(escapedDateString, DATE_FORMATTER);
				
	}
	
	private Result getResult(String fixtureJson) {
		JSONObject resultFromJson = getJsonObject(RESULT, fixtureJson); 
		
		if (resultFromJson.isNull(GOALS_HOME_TEAM) || resultFromJson.isNull(GOALS_AWAY_TEAM)) {
			return null;
		}
		
		Integer goalsHomeTeam = (Integer) resultFromJson.get(GOALS_HOME_TEAM);
		Integer goalsAwayTeam = (Integer) resultFromJson.get(GOALS_AWAY_TEAM);
		return Result.builder()
				.homeGoals(goalsHomeTeam)
				.awayGoals(goalsAwayTeam)
				.build();
	}
	
	
	
}
