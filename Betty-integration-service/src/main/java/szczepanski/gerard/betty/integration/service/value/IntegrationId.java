package szczepanski.gerard.betty.integration.service.value;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.betty.common.assertion.ParamAssertion;

/**
 * Value object that represents Id of the integration objects.
 * It can be numeric positive String only. 
 * 
 * @author Gerard Szczepanski
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class IntegrationId {
	
	private final String id;
	
	public static IntegrationId of(String id) {
		ParamAssertion.isValidStringNumericId(id);
		return new IntegrationId(id);
	}
	
	public String get() {
		return id;
	}

	@Override
	public String toString() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IntegrationId other = (IntegrationId) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
