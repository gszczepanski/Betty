package szczepanski.gerard.betty.integration.service.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import szczepanski.gerard.betty.integration.service.common.CommonIntegrationSpringConfiguration;
import szczepanski.gerard.betty.integration.service.common.IntegrationRestClient;

@Configuration
@Import(value = CommonIntegrationSpringConfiguration.class)
public class IntegrationTeamSpringConfiguration {
	
	@Autowired
	private IntegrationRestClient integrationRestClient;
	
	@Bean
	public TeamIntegrationService teamIntegrationService() {
		return new TeamIntegrationServiceImpl(integrationRestClient, teamJsonResponseMapper());
	}
	
	@Bean
	public TeamJsonResponseMapper teamJsonResponseMapper() {
		return new TeamJsonResponseMapper();
	}
	
}
