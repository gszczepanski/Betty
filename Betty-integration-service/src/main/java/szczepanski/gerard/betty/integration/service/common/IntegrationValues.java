package szczepanski.gerard.betty.integration.service.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class IntegrationValues {
	
	public static final String REST_API_HOST = "http://api.football-data.org/v1";
	
}
