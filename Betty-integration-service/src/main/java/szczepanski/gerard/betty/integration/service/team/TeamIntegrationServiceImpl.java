package szczepanski.gerard.betty.integration.service.team;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.betty.common.assertion.ParamAssertion;
import szczepanski.gerard.betty.common.logger.BettyLogger;
import szczepanski.gerard.betty.common.logger.BettyLoggerFactory;
import szczepanski.gerard.betty.integration.service.common.BettyIntegrationMappingException;
import szczepanski.gerard.betty.integration.service.common.BettyIntegrationRestException;
import szczepanski.gerard.betty.integration.service.common.IntegrationRestClient;
import szczepanski.gerard.betty.integration.service.value.IntegrationId;

@RequiredArgsConstructor
class TeamIntegrationServiceImpl implements TeamIntegrationService {

	private static final BettyLogger LOG = BettyLoggerFactory.createForName(TeamIntegrationService.class.getName());
	private static final String FIND_TEAM_PATH = "/teams/%s";

	private final IntegrationRestClient restClient;
	private final TeamJsonResponseMapper teamMapper;

	@Override
	public Set<IntegrationTeam> findByName(String name) {
		ParamAssertion.isNotBlank(name);

		return null;
	}

	@Override
	public Optional<IntegrationTeam> findOne(IntegrationId id) {
		LOG.debug("Find team for id %s", id);
		String path = String.format(FIND_TEAM_PATH, id);

		try {
			String jsonResponse = restClient.fireGetRequest(path);
			IntegrationTeam team = teamMapper.map(jsonResponse);
			return Optional.of(team);
		} catch (BettyIntegrationRestException e) {
			LOG.error("Unable to find team with id: %s", e, id);
		} catch (BettyIntegrationMappingException e) {
			LOG.error("Mapping exception occured for team with id: %s", e, id);
		}
		return Optional.empty();
	}

	@Override
	public Set<IntegrationTeam> findMany(List<IntegrationId> ids) {
		ParamAssertion.collectionNotEmpty(ids);
		LOG.debug("Find teams with ids %s", ids);
		
		Set<Optional<IntegrationTeam>> teams = ids.parallelStream().map(this::findOne).collect(Collectors.toSet());
		Set<IntegrationTeam> foundTeams = teams.stream().filter(optional -> optional.isPresent()).map(Optional::get).collect(Collectors.toSet());
		
		Set<IntegrationId> foundTeamsIds = foundTeams.stream().map(IntegrationTeam::getId).collect(Collectors.toSet());
		LOG.debug("Teams found: %s", foundTeamsIds);
		
		return foundTeams;
	}
}
