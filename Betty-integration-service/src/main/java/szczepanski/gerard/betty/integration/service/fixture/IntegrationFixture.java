package szczepanski.gerard.betty.integration.service.fixture;

import java.time.LocalDateTime;
import java.util.Optional;

import lombok.Builder;
import lombok.Getter;
import szczepanski.gerard.betty.integration.service.value.IntegrationId;

@Getter
@Builder
public class IntegrationFixture {
	
	private final IntegrationId id;
	private final IntegrationId homeTeamId;
	private final IntegrationId awayTeamId;
	private final LocalDateTime startDate;
	private final FixtureStatus status;
	private final Optional<Result> result;
	
	@Getter
	@Builder
	static class Result {
		
		private final Integer homeGoals;
		private final Integer awayGoals;
		
	}
	
	public enum FixtureStatus {
		SCHEDULED,
		TIMED,
		CANCELED,
		IN_PLAY,
		POSTPONED,
		FINISHED;
	}

}
