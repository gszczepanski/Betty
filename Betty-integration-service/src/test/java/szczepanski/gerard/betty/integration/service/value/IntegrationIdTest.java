package szczepanski.gerard.betty.integration.service.value;

import org.testng.Assert;
import org.testng.annotations.Test;

import szczepanski.gerard.betty.common.assertion.ParamAssertionException;

@Test(groups = "unit-tests")
public class IntegrationIdTest {
	
	@Test(expectedExceptions = ParamAssertionException.class)
	public void whenStringIdIsBlankThenThrowParamAssertionException() throws Exception {
		// Arrange
		String BLANK_ID = "";
		
		// Act
		IntegrationId id = IntegrationId.of(BLANK_ID);
	}
	
	@Test(expectedExceptions = ParamAssertionException.class)
	public void whenStringIdIsNullThenThrowParamAssertionException() throws Exception {
		// Arrange
		String NULL_ID = null;
		
		// Act
		IntegrationId id = IntegrationId.of(NULL_ID);
	}
	
	@Test(expectedExceptions = ParamAssertionException.class)
	public void whenStringIdIsNotNumericThenThrowParamAssertionException() throws Exception {
		// Arrange
		String NOT_NUMERIC_ID = "notNumeric";
		
		// Act
		IntegrationId id = IntegrationId.of(NOT_NUMERIC_ID);
	}
	
	@Test(expectedExceptions = ParamAssertionException.class)
	public void whenStringIdIsNegativeNumericThenThrowParamAssertionException() throws Exception {
		// Arrange
		String NEGATIVE_NUMERIC_ID = "-99";
		
		// Act
		IntegrationId id = IntegrationId.of(NEGATIVE_NUMERIC_ID);
	}
	
	@Test
	public void createValidIntegrationIdValueObject() throws Exception {
		// Arrange
		String VALID_ID = "667";
		
		// Act
		IntegrationId id = IntegrationId.of(VALID_ID);
		
		// Assert
		Assert.assertNotNull(id);
		Assert.assertEquals(id.get(), VALID_ID);
	}
	
}
