package szczepanski.gerard.betty.integration.service.fixture;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.Test;

import szczepanski.gerard.betty.common.assertion.ParamAssertionException;
import szczepanski.gerard.betty.integration.service.common.BettyIntegrationMappingException;
import szczepanski.gerard.betty.integration.service.common.BettyIntegrationRestException;
import szczepanski.gerard.betty.integration.service.common.IntegrationRestClient;
import szczepanski.gerard.betty.integration.service.value.IntegrationId;

@Test(groups = "unit-tests")
public class FixtureIntegrationServiceImplTest {
	
	@Test
	public void findOneFixtureById() throws Exception {
		//Arrange
		IntegrationRestClient restClient = Mockito.mock(IntegrationRestClient.class);
		FixtureJsonResponseMapper mapper = Mockito.mock(FixtureJsonResponseMapper.class);
		FixtureIntegrationService fixtureIntegrationService = new FixtureIntegrationServiceImpl(restClient, mapper);
		
		IntegrationId fixtureId = IntegrationId.of("123");
		String path = "/fixtures/" + fixtureId;
		String jsonResponse = "{RETURNED JSON}";
		IntegrationFixture foundFixture = IntegrationFixture.builder()
				.id(fixtureId)
				.build();
		
		Mockito.when(restClient.fireGetRequest(path)).thenReturn(jsonResponse);
		Mockito.when(mapper.map(jsonResponse)).thenReturn(foundFixture);
		
		// Act
		Optional<IntegrationFixture> optionalFoundFixture = fixtureIntegrationService.findOne(fixtureId);
		
		//Assert
		Assert.assertTrue(optionalFoundFixture.isPresent());
		Assert.assertEquals(optionalFoundFixture.get(), foundFixture);
		
		Mockito.verify(restClient).fireGetRequest(path);
		Mockito.verify(mapper).map(jsonResponse);
	}
	
	@Test
	public void whenFixtureNotFoundThenReturnEmptyOptionalObject() throws Exception {
		//Arrange
		IntegrationRestClient restClient = Mockito.mock(IntegrationRestClient.class);
		FixtureJsonResponseMapper mapper = Mockito.mock(FixtureJsonResponseMapper.class);
		FixtureIntegrationService fixtureIntegrationService = new FixtureIntegrationServiceImpl(restClient, mapper);
		
		IntegrationId fixtureId = IntegrationId.of("123");
		String path = "/fixtures/" + fixtureId;
		Mockito.when(restClient.fireGetRequest(path)).thenThrow(BettyIntegrationRestException.class);
		
		// Act
		Optional<IntegrationFixture> optionalFoundFixture = fixtureIntegrationService.findOne(fixtureId);
		
		//Assert
		Assert.assertFalse(optionalFoundFixture.isPresent());
		Mockito.verify(restClient).fireGetRequest(path);
	}
	
	@Test
	public void whenIntegrationsMappingExceptionThenReturnEmptyOptionalObject() throws Exception {
		//Arrange
		IntegrationRestClient restClient = Mockito.mock(IntegrationRestClient.class);
		FixtureJsonResponseMapper mapper = Mockito.mock(FixtureJsonResponseMapper.class);
		FixtureIntegrationService fixtureIntegrationService = new FixtureIntegrationServiceImpl(restClient, mapper);
		
		IntegrationId fixtureId = IntegrationId.of("123");
		String path = "/fixtures/" + fixtureId;
		String jsonResponse = "{RETURNED JSON}";
		Mockito.when(restClient.fireGetRequest(path)).thenReturn(jsonResponse);
		Mockito.when(mapper.map(jsonResponse)).thenThrow(BettyIntegrationMappingException.class);
		
		// Act
		Optional<IntegrationFixture> optionalFoundFixture = fixtureIntegrationService.findOne(fixtureId);
		
		//Assert
		Assert.assertFalse(optionalFoundFixture.isPresent());
		Mockito.verify(restClient).fireGetRequest(path);
	}
	
	@Test
	public void findManyFixturesByIds() throws Exception {
		//Arrange
		IntegrationRestClient restClient = Mockito.mock(IntegrationRestClient.class);
		FixtureJsonResponseMapper mapper = Mockito.mock(FixtureJsonResponseMapper.class);
		FixtureIntegrationService fixtureIntegrationService = new FixtureIntegrationServiceImpl(restClient, mapper);
		
		IntegrationId firstFixtureId = IntegrationId.of("123");
		IntegrationId secondFixtureId = IntegrationId.of("1000000");
		IntegrationId thirdFixtureId = IntegrationId.of("777");
		
		List<IntegrationId> fixturesIds = Arrays.asList(firstFixtureId, secondFixtureId, thirdFixtureId);
		int expectedFixturesSize = 2;
		
		String path = "/fixtures/";
		String firstJsonResponse = "{RETURNED FIRST JSON}";
		String thirdJsonResponse = "{RETURNED THIRD JSON}";
		
		IntegrationFixture foundFirstFixture = IntegrationFixture.builder()
				.id(firstFixtureId)
				.build();
		
		IntegrationFixture foundThirdFixture = IntegrationFixture.builder()
				.id(thirdFixtureId)
				.build();
		
		Mockito.when(restClient.fireGetRequest(path + firstFixtureId)).thenReturn(firstJsonResponse);
		Mockito.when(mapper.map(firstJsonResponse)).thenReturn(foundFirstFixture);
		
		// Second fixture not found
		Mockito.when(restClient.fireGetRequest(path + secondFixtureId)).thenThrow(BettyIntegrationRestException.class);
		
		Mockito.when(restClient.fireGetRequest(path + thirdFixtureId)).thenReturn(thirdJsonResponse);
		Mockito.when(mapper.map(thirdJsonResponse)).thenReturn(foundThirdFixture);
		
		// Act
		Set<IntegrationFixture> fixtures = fixtureIntegrationService.findMany(fixturesIds);
		
		//Assert
		Assert.assertNotNull(fixtures);
		Assert.assertFalse(fixtures.isEmpty());
		Assert.assertEquals(fixtures.size(), expectedFixturesSize);
		
		Assert.assertTrue(fixtures.contains(foundFirstFixture));
		Assert.assertTrue(fixtures.contains(foundThirdFixture));
	}
	
	@Test
	public void whenFixturesNotFoundThenReturnEmptySet() throws Exception {
		//Arrange
		IntegrationRestClient restClient = Mockito.mock(IntegrationRestClient.class);
		FixtureJsonResponseMapper mapper = Mockito.mock(FixtureJsonResponseMapper.class);
		FixtureIntegrationService fixtureIntegrationService = new FixtureIntegrationServiceImpl(restClient, mapper);
		
		IntegrationId firstFixtureId = IntegrationId.of("99");
		IntegrationId secondFixtureId = IntegrationId.of("99");
		IntegrationId thirdFixtureId = IntegrationId.of("99");
		
		List<IntegrationId> fixturesIds = Arrays.asList(firstFixtureId, secondFixtureId, thirdFixtureId);
		int expectedFixturesSize = 0;
		
		String path = "/fixtures/99";
		Mockito.when(restClient.fireGetRequest(path)).thenThrow(BettyIntegrationRestException.class);
		
		// Act
		Set<IntegrationFixture> fixtures = fixtureIntegrationService.findMany(fixturesIds);
		
		//Assert
		Assert.assertNotNull(fixtures);
		Assert.assertTrue(fixtures.isEmpty());
		Assert.assertEquals(fixtures.size(), expectedFixturesSize);
	}
	
	@Test(expectedExceptions = ParamAssertionException.class)
	public void whenEmptyIntegrationIdsListIsPassedToFindManyMethodThenThrowException() throws Exception {
		//Arrange
		IntegrationRestClient restClient = Mockito.mock(IntegrationRestClient.class);
		FixtureJsonResponseMapper mapper = Mockito.mock(FixtureJsonResponseMapper.class);
		FixtureIntegrationService fixtureIntegrationService = new FixtureIntegrationServiceImpl(restClient, mapper);
		
		List<IntegrationId> EMPTY_FIXTURES_IDS = new ArrayList<>();
		
		// Act
		fixtureIntegrationService.findMany(EMPTY_FIXTURES_IDS);
	}
	
}
