package szczepanski.gerard.betty.integration.service.common;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.github.kristofa.test.http.Method;
import com.github.kristofa.test.http.MockHttpServer;
import com.github.kristofa.test.http.SimpleHttpResponseProvider;

import szczepanski.gerard.betty.common.assertion.ParamAssertionException;

@Test(groups = "unit-tests")
public class SimpleIntegrationRestClientTest {
	
	MockHttpServer server = null;

	@Test(expectedExceptions = ParamAssertionException.class)
	public void whenHostNameIsBlankThrowParamAssertion() {
		// Arrange
		String BLANK_HOST_NAME = "";

		// Act
		SimpleIntegrationRestClient.createForHost(BLANK_HOST_NAME);
	}

	@Test(expectedExceptions = ParamAssertionException.class)
	public void whenHostNameIsNullThrowParamAssertion() {
		// Arrange
		String NULL_HOST_NAME = null;

		// Act
		SimpleIntegrationRestClient.createForHost(NULL_HOST_NAME);
	}

	@Test
	public void fireGetRequestReturnsMessage() throws Exception {
		// Arrange
		Integer WEB_SERVICE_PORT = 6666;
		String MOCKED_WEB_SERVICE_HOST = "http://localhost:" + WEB_SERVICE_PORT;
		String REQUEST_PATH = "/team/1";
		String expectedJsonResponse = "{\"message\":\"Hello\"}";
		
		SimpleHttpResponseProvider responseProvider = new SimpleHttpResponseProvider();
		responseProvider.expect(Method.GET, REQUEST_PATH)
						.respondWith(200, "text/json", expectedJsonResponse);
		
		server = new MockHttpServer(WEB_SERVICE_PORT, responseProvider);
		IntegrationRestClient client = SimpleIntegrationRestClient.createForHost(MOCKED_WEB_SERVICE_HOST);
		
		// Act
		server.start();
		String response = client.fireGetRequest(REQUEST_PATH);

		// Assert
		Assert.assertNotNull(response);
		Assert.assertEquals(response, expectedJsonResponse);
	}
	
	@Test(expectedExceptions = BettyIntegrationRestException.class)
	public void fireGetRequestWhenResourceNotFoundThrowsIntegrtionException() throws Exception {
		// Arrange
		Integer WEB_SERVICE_PORT = 6666;
		String MOCKED_WEB_SERVICE_HOST = "http://localhost:" + WEB_SERVICE_PORT;
		String REQUEST_PATH = "/team/666";
		String resourceNotFoundResponse = "Resource not found";
		
		SimpleHttpResponseProvider responseProvider = new SimpleHttpResponseProvider();
		responseProvider.expect(Method.GET, REQUEST_PATH)
						.respondWith(404, "text/json", resourceNotFoundResponse);
		
		server = new MockHttpServer(WEB_SERVICE_PORT, responseProvider);
		IntegrationRestClient client = SimpleIntegrationRestClient.createForHost(MOCKED_WEB_SERVICE_HOST);
		
		// Act
		server.start();
		client.fireGetRequest(REQUEST_PATH);
	}
	
	@Test(expectedExceptions = BettyIntegrationRestException.class)
	public void whenServerNotResponseFireGetRequestMethodThrowsError() throws Exception {
		// Arrange
		String MOCKED_HOST = "http://localhost:6666";
		String REQUEST_PATH = "/team/1";
		IntegrationRestClient client = SimpleIntegrationRestClient.createForHost(MOCKED_HOST);
		
		// Act
		client.fireGetRequest(REQUEST_PATH);
	}
	
	@AfterMethod
	public void tearDown() throws Exception {
		if (server != null) {
			server.stop();
			server = null;
		}
	}

}
