package szczepanski.gerard.betty.integration.service.fixture;

import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import szczepanski.gerard.betty.integration.service.common.BettyIntegrationMappingException;
import szczepanski.gerard.betty.integration.service.fixture.IntegrationFixture.FixtureStatus;
import szczepanski.gerard.betty.integration.service.fixture.IntegrationFixture.Result;
import szczepanski.gerard.betty.integration.service.value.IntegrationId;

@Test(groups = "unit-tests")
public class FixtureJsonResponseMapperTest {

	@Test(expectedExceptions = BettyIntegrationMappingException.class)
	public void whenJsonIsBlankThenThrowException() throws Exception {
		// Arrange
		String BLANK_JSON = "";
		FixtureJsonResponseMapper mapper = new FixtureJsonResponseMapper();

		// Act
		mapper.map(BLANK_JSON);
	}

	@Test(expectedExceptions = BettyIntegrationMappingException.class)
	public void whenJsonIsNullThenThrowException() throws Exception {
		// Arrange
		String NULL_JSON = null;
		FixtureJsonResponseMapper mapper = new FixtureJsonResponseMapper();

		// Act
		mapper.map(NULL_JSON);
	}
	
	@Test(expectedExceptions = BettyIntegrationMappingException.class)
	public void whenJsonIsBrokenThenThrowException() throws Exception {
		// Arrange
		String invalidJson = "{\"fixture\":{\"_links\":{\"self\":{\"href\":\"http://api.football-data.org/v1/fixtures/159324\"},\"competition\":{\"href\":\"http://api.football-data.org/v1/competitions/445\"},\"homeTeam\":{\"href\":\"http://api.football-data.org/v1/teams/346\"}},\"date\":\"2017-08-12T11:30:00Z\",\"status\":\"FINISHED\",\"matchday\":1,\"homeTeamName\":\"Watford FC\",\"awayTeamName\":\"Liverpool FC\",\"result\":{\"goalsHomeTeam\":2,\"goalsAwayTeam\":0},\"odds\":null}}";
		FixtureJsonResponseMapper mapper = new FixtureJsonResponseMapper();

		// Act
		mapper.map(invalidJson);
	}

	@Test
	public void mapNotFinishedFixtureTest() throws Exception {
		// Arrange
		String json = "{\"fixture\":{\"_links\":{\"self\":{\"href\":\"http://api.football-data.org/v1/fixtures/159324\"},\"competition\":{\"href\":\"http://api.football-data.org/v1/competitions/445\"},\"homeTeam\":{\"href\":\"http://api.football-data.org/v1/teams/346\"},\"awayTeam\":{\"href\":\"http://api.football-data.org/v1/teams/64\"}},\"date\":\"2017-08-12T11:30:00Z\",\"status\":\"TIMED\",\"matchday\":1,\"homeTeamName\":\"Watford FC\",\"awayTeamName\":\"Liverpool FC\",\"result\":{\"goalsHomeTeam\":null,\"goalsAwayTeam\":null},\"odds\":null}}";

		IntegrationId expectedId = IntegrationId.of("159324");
		IntegrationId expectedHomeTeamId = IntegrationId.of("346");
		IntegrationId expectedAwayTeamId = IntegrationId.of("64");
		LocalDateTime expectedStartDate = LocalDateTime.of(2017, 8, 12, 11, 30);
		FixtureStatus expectedStatus = FixtureStatus.TIMED;

		FixtureJsonResponseMapper mapper = new FixtureJsonResponseMapper();

		// Act
		IntegrationFixture fixture = mapper.map(json);

		// Assert
		Assert.assertNotNull(fixture);
		Assert.assertEquals(fixture.getId(), expectedId);
		Assert.assertEquals(fixture.getHomeTeamId(), expectedHomeTeamId);
		Assert.assertEquals(fixture.getAwayTeamId(), expectedAwayTeamId);
		Assert.assertEquals(fixture.getStartDate(), expectedStartDate);
		Assert.assertEquals(fixture.getStatus(), expectedStatus);
		Assert.assertFalse(fixture.getResult().isPresent());
	}

	@Test
	public void mapFinishedFixtureTest() throws Exception {
		// Arrange
		String json = "{\"fixture\":{\"_links\":{\"self\":{\"href\":\"http://api.football-data.org/v1/fixtures/159324\"},\"competition\":{\"href\":\"http://api.football-data.org/v1/competitions/445\"},\"homeTeam\":{\"href\":\"http://api.football-data.org/v1/teams/346\"},\"awayTeam\":{\"href\":\"http://api.football-data.org/v1/teams/64\"}},\"date\":\"2017-08-12T11:30:00Z\",\"status\":\"FINISHED\",\"matchday\":1,\"homeTeamName\":\"Watford FC\",\"awayTeamName\":\"Liverpool FC\",\"result\":{\"goalsHomeTeam\":2,\"goalsAwayTeam\":0},\"odds\":null}}";

		IntegrationId expectedId = IntegrationId.of("159324");
		IntegrationId expectedHomeTeamId = IntegrationId.of("346");
		IntegrationId expectedAwayTeamId = IntegrationId.of("64");
		LocalDateTime expectedStartDate = LocalDateTime.of(2017, 8, 12, 11, 30);
		FixtureStatus expectedStatus = FixtureStatus.FINISHED;
		Integer expectedHomeGoals = 2;
		Integer expectedAwayGoals = 0;

		FixtureJsonResponseMapper mapper = new FixtureJsonResponseMapper();

		// Act
		IntegrationFixture fixture = mapper.map(json);

		// Assert
		Assert.assertNotNull(fixture);
		Assert.assertEquals(fixture.getId(), expectedId);
		Assert.assertEquals(fixture.getHomeTeamId(), expectedHomeTeamId);
		Assert.assertEquals(fixture.getAwayTeamId(), expectedAwayTeamId);
		Assert.assertEquals(fixture.getStartDate(), expectedStartDate);
		Assert.assertEquals(fixture.getStatus(), expectedStatus);
		Assert.assertTrue(fixture.getResult().isPresent());

		Result result = fixture.getResult().get();
		Assert.assertEquals(result.getHomeGoals(), expectedHomeGoals);
		Assert.assertEquals(result.getAwayGoals(), expectedAwayGoals);
	}
	
}
