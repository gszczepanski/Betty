package szczepanski.gerard.betty.integration.service.fixture;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import szczepanski.gerard.betty.integration.service.common.IntegrationRestClient;
import szczepanski.gerard.betty.integration.service.common.IntegrationValues;
import szczepanski.gerard.betty.integration.service.common.SimpleIntegrationRestClient;
import szczepanski.gerard.betty.integration.service.value.IntegrationId;

@Test(groups = "integration-tests")
public class FixtureIntegrationServiceImplIntegrationTest {
	
	FixtureIntegrationService fixtureIntegrationService;
	
	@BeforeMethod
	public void beforeMethod() {
		IntegrationRestClient restClient = SimpleIntegrationRestClient.createForHost(IntegrationValues.REST_API_HOST);
		FixtureJsonResponseMapper mapper = new FixtureJsonResponseMapper();
		fixtureIntegrationService = new FixtureIntegrationServiceImpl(restClient, mapper);
	}
	
	@Test
	public void findOneTeamById() throws Exception {
		// Arrange
		IntegrationId fixtureId = IntegrationId.of("159324");
		
		// Act
		Optional<IntegrationFixture> optionalFoundFixture = fixtureIntegrationService.findOne(fixtureId);
		
		//Assert
		Assert.assertTrue(optionalFoundFixture.isPresent());
		
		IntegrationFixture foundFixture = optionalFoundFixture.get();
		Assert.assertNotNull(foundFixture.getId());
	}
	
	@Test
	public void whenTeamNotFoundThenReturnEmptyOptionalObject() throws Exception {
		// Arrange
		IntegrationId FAKE_FIXTURE_ID = IntegrationId.of("111");
		
		// Act
		Optional<IntegrationFixture> optionalFoundFixture = fixtureIntegrationService.findOne(FAKE_FIXTURE_ID);
		
		//Assert
		Assert.assertFalse(optionalFoundFixture.isPresent());
	}
	
	@Test
	public void findManyByIds() throws Exception {
		// Arrange
		List<IntegrationId> fixturesIds = Arrays.asList(
					IntegrationId.of("159324"), 
					IntegrationId.of("159325"), 
					IntegrationId.of("159326"), 
					IntegrationId.of("159327"));
		
		int expectedFixturesFoundSize = 4;
		
		// Act
		Set<IntegrationFixture> fixturesFound = fixtureIntegrationService.findMany(fixturesIds);
		
		//Assert
		Assert.assertFalse(fixturesFound.isEmpty());
		Assert.assertEquals(fixturesFound.size(), expectedFixturesFoundSize);
	}
	
	
}
