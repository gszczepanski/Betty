package szczepanski.gerard.betty.integration.service.team;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import szczepanski.gerard.betty.integration.service.common.IntegrationRestClient;
import szczepanski.gerard.betty.integration.service.common.IntegrationValues;
import szczepanski.gerard.betty.integration.service.common.SimpleIntegrationRestClient;
import szczepanski.gerard.betty.integration.service.value.IntegrationId;

@Test(groups = "integration-tests")
public class TeamIntegrationServiceImplIntegrationTest {
	
	TeamIntegrationService teamIntegrationService;
	
	@BeforeMethod
	public void beforeMethod() {
		IntegrationRestClient restClient = SimpleIntegrationRestClient.createForHost(IntegrationValues.REST_API_HOST);
		TeamJsonResponseMapper mapper = new TeamJsonResponseMapper();
		teamIntegrationService = new TeamIntegrationServiceImpl(restClient, mapper);
	}
	
	@Test
	public void findOneTeamById() throws Exception {
		// Arrange
		IntegrationId teamId = IntegrationId.of("66");
		
		// Act
		Optional<IntegrationTeam> optionalFoundTeam = teamIntegrationService.findOne(teamId);
		
		//Assert
		Assert.assertTrue(optionalFoundTeam.isPresent());
		
		IntegrationTeam foundTeam = optionalFoundTeam.get();
		Assert.assertNotNull(foundTeam.getId());
	}
	
	@Test
	public void whenTeamNotFoundThenReturnEmptyOptionalObject() throws Exception {
		// Arrange
		IntegrationId FAKE_TEAM_ID = IntegrationId.of("70000");
		
		// Act
		Optional<IntegrationTeam> optionalFoundTeam = teamIntegrationService.findOne(FAKE_TEAM_ID);
		
		//Assert
		Assert.assertFalse(optionalFoundTeam.isPresent());
	}
	
	@Test
	public void findManyByIds() throws Exception {
		// Arrange
		List<IntegrationId> teamsIds = Arrays.asList(
					IntegrationId.of("66"), 
					IntegrationId.of("67"), 
					IntegrationId.of("68"), 
					IntegrationId.of("69"));
		
		int expectedTeamsFoundSize = 4;
		
		// Act
		Set<IntegrationTeam> teamsFound = teamIntegrationService.findMany(teamsIds);
		
		//Assert
		Assert.assertFalse(teamsFound.isEmpty());
		Assert.assertEquals(teamsFound.size(), expectedTeamsFoundSize);
	}
	
	
}
