package szczepanski.gerard.betty.integration.service.team;

import org.testng.Assert;
import org.testng.annotations.Test;

import szczepanski.gerard.betty.integration.service.common.BettyIntegrationMappingException;
import szczepanski.gerard.betty.integration.service.value.IntegrationId;

@Test(groups = "unit-tests")
public class TeamJsonResponseMapperTest {

	@Test(expectedExceptions = BettyIntegrationMappingException.class)
	public void whenJsonIsBlankThenThrowException() throws Exception {
		// Arrange
		String BLANK_JSON = "";
		TeamJsonResponseMapper mapper = new TeamJsonResponseMapper();

		// Act
		mapper.map(BLANK_JSON);
	}

	@Test(expectedExceptions = BettyIntegrationMappingException.class)
	public void whenJsonIsNullThenThrowException() throws Exception {
		// Arrange
		String NULL_JSON = null;
		TeamJsonResponseMapper mapper = new TeamJsonResponseMapper();

		// Act
		mapper.map(NULL_JSON);
	}

	@Test(expectedExceptions = BettyIntegrationMappingException.class)
	public void whenJsonIsBrokenThenThrowException() throws Exception {
		// Arrange
		String invalidJson = "{\"_links\":{\"self\":{\"href\":\"http://api.football-data.org/v1/teams/66\"},\"fixtures\":{\"href\":\"http://api.football-data.org/v1/teams/66/fixtures\"},\"players\":{\"href\":\"http://api.football-data.org/v1/teams/66/players\"}},\"shortName\":\"ManU\",\"squadMarketValue\":\"377,250,000 �\",\"crestUrl\":\"http://upload.wikimedia.org/wikipedia/de/d/da/Manchester_United_FC.svg\"}";
		TeamJsonResponseMapper mapper = new TeamJsonResponseMapper();

		// Act
		mapper.map(invalidJson);
	}
	
	@Test
	public void convertJsonResponseToTeamObject() throws Exception {
		// Arrange
		String json = "{\"_links\":{\"self\":{\"href\":\"http://api.football-data.org/v1/teams/66\"},\"fixtures\":{\"href\":\"http://api.football-data.org/v1/teams/66/fixtures\"},\"players\":{\"href\":\"http://api.football-data.org/v1/teams/66/players\"}},\"name\":\"Manchester United FC\",\"shortName\":\"ManU\",\"squadMarketValue\":\"377,250,000 �\",\"crestUrl\":\"http://upload.wikimedia.org/wikipedia/de/d/da/Manchester_United_FC.svg\"}";
		IntegrationId expectedId = IntegrationId.of("66");
		String expectedName = "Manchester United FC";
		String expectedImageURL = "http://upload.wikimedia.org/wikipedia/de/d/da/Manchester_United_FC.svg";

		TeamJsonResponseMapper mapper = new TeamJsonResponseMapper();

		// Act
		IntegrationTeam team = mapper.map(json);

		// Assert
		Assert.assertNotNull(team);
		Assert.assertEquals(team.getId(), expectedId);
		Assert.assertEquals(team.getName(), expectedName);
		Assert.assertEquals(team.getImageURL(), expectedImageURL);
	}
	

}
