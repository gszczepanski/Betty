package szczepanski.gerard.betty.integration.service.team;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.Test;

import szczepanski.gerard.betty.common.assertion.ParamAssertionException;
import szczepanski.gerard.betty.integration.service.common.BettyIntegrationMappingException;
import szczepanski.gerard.betty.integration.service.common.BettyIntegrationRestException;
import szczepanski.gerard.betty.integration.service.common.IntegrationRestClient;
import szczepanski.gerard.betty.integration.service.value.IntegrationId;

@Test(groups = "unit-tests")
public class TeamIntegrationServiceImplTest {
	
	@Test
	public void findOneTeamById() throws Exception {
		// Arrange
		IntegrationRestClient restClient = Mockito.mock(IntegrationRestClient.class);
		TeamJsonResponseMapper mapper = Mockito.mock(TeamJsonResponseMapper.class);
		TeamIntegrationService teamIntegrationService = new TeamIntegrationServiceImpl(restClient, mapper);
		
		IntegrationId teamId = IntegrationId.of("12");
		String path = "/teams/" + teamId;
		String jsonResponse = "{\"name\":\"Juventus\"}";
		IntegrationTeam createdTeam = IntegrationTeam.builder()
				.name("Juventus")
				.build();
		
		Mockito.when(restClient.fireGetRequest(path)).thenReturn(jsonResponse);
		Mockito.when(mapper.map(jsonResponse)).thenReturn(createdTeam);
		
		// Act
		Optional<IntegrationTeam> optionalFoundTeam = teamIntegrationService.findOne(teamId);
		
		//Assert
		Assert.assertTrue(optionalFoundTeam.isPresent());
		Assert.assertEquals(optionalFoundTeam.get(), createdTeam);
		
		Mockito.verify(restClient).fireGetRequest(path);
		Mockito.verify(mapper).map(jsonResponse);
	}
	
	@Test
	public void whenTeamNotFoundThenReturnEmptyOptionalObject() throws Exception {
		// Arrange
		IntegrationRestClient restClient = Mockito.mock(IntegrationRestClient.class);
		TeamJsonResponseMapper mapper = Mockito.mock(TeamJsonResponseMapper.class);
		TeamIntegrationService teamIntegrationService = new TeamIntegrationServiceImpl(restClient, mapper);
		
		IntegrationId teamId = IntegrationId.of("12");
		String path = "/teams/" + teamId;
		
		Mockito.when(restClient.fireGetRequest(path)).thenThrow(BettyIntegrationRestException.class);
		
		// Act
		Optional<IntegrationTeam> optionalFoundTeam = teamIntegrationService.findOne(teamId);
		
		//Assert
		Assert.assertFalse(optionalFoundTeam.isPresent());
		Mockito.verify(restClient).fireGetRequest(path);
	}
	
	@Test
	public void whenIntegrationMappingExceptionThenReturnEmptyOptionalObject() throws Exception {
		// Arrange
		IntegrationRestClient restClient = Mockito.mock(IntegrationRestClient.class);
		TeamJsonResponseMapper mapper = Mockito.mock(TeamJsonResponseMapper.class);
		TeamIntegrationService teamIntegrationService = new TeamIntegrationServiceImpl(restClient, mapper);
		
		IntegrationId teamId = IntegrationId.of("12");
		String path = "/teams/" + teamId;
		String invalidJson = "{invalid}";
		
		Mockito.when(restClient.fireGetRequest(path)).thenReturn(invalidJson);
		Mockito.when(mapper.map(invalidJson)).thenThrow(BettyIntegrationMappingException.class);
		
		// Act
		Optional<IntegrationTeam> optionalFoundTeam = teamIntegrationService.findOne(teamId);
		
		//Assert
		Assert.assertFalse(optionalFoundTeam.isPresent());
		Mockito.verify(restClient).fireGetRequest(path);
	}
	
	@Test
	public void findManyTeamsByIds() throws Exception {
		//Arrange
		IntegrationRestClient restClient = Mockito.mock(IntegrationRestClient.class);
		TeamJsonResponseMapper mapper = Mockito.mock(TeamJsonResponseMapper.class);
		TeamIntegrationService teamIntegrationService = new TeamIntegrationServiceImpl(restClient, mapper);
		
		IntegrationId firstTeamId = IntegrationId.of("123");
		IntegrationId secondTeamId = IntegrationId.of("1000000");
		IntegrationId thirdTeamId = IntegrationId.of("777");
		
		List<IntegrationId> teamsIds = Arrays.asList(firstTeamId, secondTeamId, thirdTeamId);
		int expectedTeamsSize = 2;
		
		String path = "/teams/";
		String firstJsonResponse = "{RETURNED FIRST JSON}";
		String thirdJsonResponse = "{RETURNED THIRD JSON}";
		
		IntegrationTeam foundFirstTeam = IntegrationTeam.builder()
				.id(firstTeamId)
				.build();
		
		IntegrationTeam foundThirdTeam = IntegrationTeam.builder()
				.id(thirdTeamId)
				.build();
		
		Mockito.when(restClient.fireGetRequest(path + firstTeamId)).thenReturn(firstJsonResponse);
		Mockito.when(mapper.map(firstJsonResponse)).thenReturn(foundFirstTeam);
		
		// Second fixture not found
		Mockito.when(restClient.fireGetRequest(path + secondTeamId)).thenThrow(BettyIntegrationRestException.class);
		
		Mockito.when(restClient.fireGetRequest(path + thirdTeamId)).thenReturn(thirdJsonResponse);
		Mockito.when(mapper.map(thirdJsonResponse)).thenReturn(foundThirdTeam);
		
		// Act
		Set<IntegrationTeam> teams = teamIntegrationService.findMany(teamsIds);
		
		//Assert
		Assert.assertNotNull(teams);
		Assert.assertFalse(teams.isEmpty());
		Assert.assertEquals(teams.size(), expectedTeamsSize);
		
		Assert.assertTrue(teams.contains(foundFirstTeam));
		Assert.assertTrue(teams.contains(foundThirdTeam));
	}
	
	@Test
	public void whenTeamsNotFoundThenReturnEmptySet() throws Exception {
		//Arrange
		IntegrationRestClient restClient = Mockito.mock(IntegrationRestClient.class);
		TeamJsonResponseMapper mapper = Mockito.mock(TeamJsonResponseMapper.class);
		TeamIntegrationService teamIntegrationService = new TeamIntegrationServiceImpl(restClient, mapper);
		
		IntegrationId firstTeamId = IntegrationId.of("99");
		IntegrationId secondTeamId = IntegrationId.of("99");
		IntegrationId thirdTeamId = IntegrationId.of("99");
		
		List<IntegrationId> teamIds = Arrays.asList(firstTeamId, secondTeamId, thirdTeamId);
		int expectedTeamsSize = 0;
		
		String path = "/teams/99";
		Mockito.when(restClient.fireGetRequest(path)).thenThrow(BettyIntegrationRestException.class);
		
		// Act
		Set<IntegrationTeam> teams = teamIntegrationService.findMany(teamIds);
		
		//Assert
		Assert.assertNotNull(teams);
		Assert.assertTrue(teams.isEmpty());
		Assert.assertEquals(teams.size(), expectedTeamsSize);
	}
	
	@Test(expectedExceptions = ParamAssertionException.class)
	public void whenEmptyIntegrationIdsListIsPassedToFindManyMethodThenThrowException() throws Exception {
		//Arrange
		IntegrationRestClient restClient = Mockito.mock(IntegrationRestClient.class);
		TeamJsonResponseMapper mapper = Mockito.mock(TeamJsonResponseMapper.class);
		TeamIntegrationService teamIntegrationService = new TeamIntegrationServiceImpl(restClient, mapper);
		
		List<IntegrationId> EMPTY_TEAM_IDS = new ArrayList<>();
		
		// Act
		teamIntegrationService.findMany(EMPTY_TEAM_IDS);
	}
	
}
