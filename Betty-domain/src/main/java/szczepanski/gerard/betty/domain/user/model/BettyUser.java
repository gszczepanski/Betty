package szczepanski.gerard.betty.domain.user.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import szczepanski.gerard.betty.domain.common.Schema;
import szczepanski.gerard.betty.domain.common.model.AbstractEntity;

@Getter
@Entity
@Table(schema = Schema.USER, name = "BETTY_USER")
public class BettyUser extends AbstractEntity {
	
	@OneToOne
	@JoinColumn(name = "CREDENTIALS_ID")
	private Credentials credentials;
	
	@Column(name = "EMAIL")
	private String email;
	
	
}
