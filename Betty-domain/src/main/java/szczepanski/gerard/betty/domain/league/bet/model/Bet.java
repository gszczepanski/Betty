package szczepanski.gerard.betty.domain.league.bet.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import szczepanski.gerard.betty.domain.common.Schema;
import szczepanski.gerard.betty.domain.common.model.AbstractEntity;
import szczepanski.gerard.betty.domain.league.round.model.RoundFixture;

@Entity
@Table(schema = Schema.BETTING, name = "BET")
@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Bet extends AbstractEntity {
	
	@Column(name = "CODE")
	private String code;
	
	@ManyToOne
	@JoinColumn(name = "ROUND_BET_ID")
	private RoundBet roundBet;
	
	@ManyToOne
	@JoinColumn(name = "FIXTURE_ID")
	private RoundFixture fixture;
	
	@Column(name = "HOME_SCORE_BET")
	private Integer homeScoreBet;
	
	@Column(name = "AWAY_SCORE_BET")
	private Integer awayScoreBet;
	
	@Column(name = "POINTS_GAINED")
	private Integer pointsGained;
	
}
