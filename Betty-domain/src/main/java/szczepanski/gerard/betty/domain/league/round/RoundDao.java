package szczepanski.gerard.betty.domain.league.round;

import org.springframework.data.jpa.repository.JpaRepository;

import szczepanski.gerard.betty.domain.league.round.model.Round;

public interface RoundDao extends JpaRepository<Round, Long> {

}
