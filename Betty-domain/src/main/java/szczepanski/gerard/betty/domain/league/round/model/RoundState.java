package szczepanski.gerard.betty.domain.league.round.model;

/**
 * Represents round state.
 * 
 * OPEN -> before first fixture date. Booking enabled.
 * IN_PROGRESS -> after first fixture date. Booking disabled.
 * FINISHED -> after last fixture date. Ready for results counted.
 * CLOSED -> results fetched, but season not updated.
 * COUNTED -> season updated, round is closed.
 * @author Gerard Szczepanski
 */
public enum RoundState {
	
	OPEN,
	IN_PROGRESS,
	FINISHED,
	CLOSED,
	COUNTED;
	
}
