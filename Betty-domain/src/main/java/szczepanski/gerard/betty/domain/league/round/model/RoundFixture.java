package szczepanski.gerard.betty.domain.league.round.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import szczepanski.gerard.betty.domain.common.Schema;
import szczepanski.gerard.betty.domain.common.model.AbstractEntity;

@Entity
@Table(schema = Schema.BETTING, name = "ROUND_FIXTURE")
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RoundFixture extends AbstractEntity {
	
	@Column(name = "CODE")
	private String code;
	
	@ManyToOne
	@JoinColumn(name = "ROUND")
	private Round round;
	
	@Column(name = "START_DATE")
	private LocalDateTime startDate;
	
	@Column(name = "END_DATE")
	private LocalDateTime endDate;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "TYPE")
	private RoundFixtureType type;
	
	@Column(name = "FIXTURE_ORDER")
	private Integer order;
	
	@Column(name = "SYNCHRONIZATION_FIXTURE_ID")
	private String synchronizationFixtureId;
	
	@OneToOne
	@JoinColumn(name = "HOME_TEAM_ID")
	private FixtureTeam homeTeam;
	
	@OneToOne
	@JoinColumn(name = "AWAY_TEAM_ID")
	private FixtureTeam awayTeam;
	
	@Column(name = "HOME_SCORE_RESULT")
	private Integer homeScoreResult;
	
	@Column(name = "AWAY_SCORE_RESULT")
	private Integer awayScoreResult;
	
	public RoundFixture(Long id) {
		setId(id);
	}
	
}
