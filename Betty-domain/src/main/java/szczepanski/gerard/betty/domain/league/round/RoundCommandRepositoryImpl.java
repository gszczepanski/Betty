package szczepanski.gerard.betty.domain.league.round;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.betty.common.assertion.ParamAssertion;
import szczepanski.gerard.betty.domain.league.round.model.Round;

@Builder
@RequiredArgsConstructor
public class RoundCommandRepositoryImpl implements RoundCommandRepository {
	
	private final RoundDao roundDao;
	
	@Override
	public void create(Round newRound) {
		ParamAssertion.isNotNull(newRound);
		roundDao.save(newRound);
	}

}
