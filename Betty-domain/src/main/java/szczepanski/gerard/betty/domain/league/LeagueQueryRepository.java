package szczepanski.gerard.betty.domain.league;

public interface LeagueQueryRepository {
	
	Long getLeagueIdBySeasonId(Long seasonId);
	
	Boolean isUserOwnerOfLeague(Long userId, Long leagueId);
	
}
