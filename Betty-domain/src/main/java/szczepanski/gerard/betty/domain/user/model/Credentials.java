package szczepanski.gerard.betty.domain.user.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import szczepanski.gerard.betty.domain.common.Schema;
import szczepanski.gerard.betty.domain.common.model.AbstractEntity;

@Getter
@Entity
@Table(schema = Schema.USER, name = "CREDENTIALS")
public class Credentials extends AbstractEntity {
	
	@Column(name = "USERNAME")
	private String username;
	
	@Column(name = "PASS")
	private String pass;
	
}
