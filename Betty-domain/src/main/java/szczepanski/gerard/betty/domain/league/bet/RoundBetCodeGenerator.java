package szczepanski.gerard.betty.domain.league.bet;

import java.util.Optional;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.betty.domain.common.AbstractCodeGenerator;

@Builder
@RequiredArgsConstructor
public class RoundBetCodeGenerator extends AbstractCodeGenerator {
	
	private static final String CODE_PREFIX = "RBET-";
	private static final String CODE_PATTERN = "%06d";
	private static final String INIT_CODE = "RBET-000001";
	
	private final RoundBetQueryRepository roundBetQueryRepository;
	
	@Override
	public String nextCode() {
		Optional<String> optionalLatestRoundBetCode = roundBetQueryRepository.getLatestRoundBetCode();
		
		if (optionalLatestRoundBetCode.isPresent()) {
			String latestBetCode = optionalLatestRoundBetCode.get();
			return getIncrementedCode(latestBetCode, CODE_PREFIX, CODE_PATTERN);
		}
		return INIT_CODE;
	}
	

}
