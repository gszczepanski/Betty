package szczepanski.gerard.betty.domain.league.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import szczepanski.gerard.betty.domain.common.Schema;
import szczepanski.gerard.betty.domain.common.model.AbstractEntity;
import szczepanski.gerard.betty.domain.user.model.BettyUser;

@Entity
@Table(schema = Schema.BETTING, name = "LEAGUE")
public class League extends AbstractEntity {

	@Column(name = "CODE")
	private String code;

	@ManyToOne
	@JoinColumn(name = "ADMIN_BETTY_USER_ID")
	private BettyUser admin;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "LEAGUE_PLAYERS_JOIN_TABLE", joinColumns = { @JoinColumn(name = "LEAGUE_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "BETTY_USER_ID") })
	private Set<BettyUser> players;

}
