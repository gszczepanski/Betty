package szczepanski.gerard.betty.domain.league.bet;

import szczepanski.gerard.betty.domain.league.bet.model.RoundBet;

public interface RoundBetCommandRepository {
	
	void create(RoundBet roundBet);
	
}
