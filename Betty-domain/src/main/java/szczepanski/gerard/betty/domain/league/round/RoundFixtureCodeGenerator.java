package szczepanski.gerard.betty.domain.league.round;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.betty.domain.common.AbstractCodeGenerator;

@Builder
@RequiredArgsConstructor
public class RoundFixtureCodeGenerator extends AbstractCodeGenerator {

	private static final String CODE_PREFIX = "FIX-";
	private static final String CODE_PATTERN = "%06d";
	private static final String INIT_CODE = "FIX-000000";

	private final RoundQueryRepository roundQueryRepository;

	@Override
	public List<String> nextCodes(int nextCodesNumber) {
		Optional<String> optionalLatestRoundFixtureCode = roundQueryRepository.getLatestRoundFixtureCode();
		
		if (optionalLatestRoundFixtureCode.isPresent()) {
			String latestRoundFixtureCode = optionalLatestRoundFixtureCode.get();
			return generateCodes(latestRoundFixtureCode, nextCodesNumber);
		}
		
		return generateCodes(INIT_CODE, nextCodesNumber);
	}

	private List<String> generateCodes(String latestCode, int numberOfCodesToGenerate) {
		List<String> generatedCodes = new ArrayList<>(numberOfCodesToGenerate);
		
		for (int i = 0; i < numberOfCodesToGenerate; i++) {
			String newCode = getIncrementedCode(latestCode, CODE_PREFIX, CODE_PATTERN);
			generatedCodes.add(newCode);
			latestCode = newCode;
		}
		
		return generatedCodes;
	}

}
