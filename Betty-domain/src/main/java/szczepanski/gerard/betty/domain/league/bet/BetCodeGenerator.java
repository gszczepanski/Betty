package szczepanski.gerard.betty.domain.league.bet;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.betty.domain.common.AbstractCodeGenerator;

@Builder
@RequiredArgsConstructor
public class BetCodeGenerator extends AbstractCodeGenerator {

	private static final String CODE_PREFIX = "BET-";
	private static final String CODE_PATTERN = "%06d";
	private static final String INIT_CODE = "BET-000000";

	private final RoundBetQueryRepository roundBetQueryRepository;

	@Override
	public List<String> nextCodes(int nextCodesNumber) {
		Optional<String> optionalLatestBetCode = roundBetQueryRepository.getLatestBetCode();
		
		if (optionalLatestBetCode.isPresent()) {
			String latestBetCode = optionalLatestBetCode.get();
			return generateCodes(latestBetCode, nextCodesNumber);
		}
		
		return generateCodes(INIT_CODE, nextCodesNumber);
	}

	private List<String> generateCodes(String latestCode, int numberOfCodesToGenerate) {
		List<String> generatedCodes = new ArrayList<>(numberOfCodesToGenerate);
		
		for (int i = 0; i < numberOfCodesToGenerate; i++) {
			String newCode = getIncrementedCode(latestCode, CODE_PREFIX, CODE_PATTERN);
			generatedCodes.add(newCode);
			latestCode = newCode;
		}
		
		return generatedCodes;
	}

}
