package szczepanski.gerard.betty.domain.league.round;

import java.util.Optional;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.betty.domain.common.AbstractCodeGenerator;

@Builder
@RequiredArgsConstructor
public class RoundCodeGenerator extends AbstractCodeGenerator {
	
	private static final String CODE_PREFIX = "RD-";
	private static final String CODE_PATTERN = "%06d";
	private static final String INIT_CODE = "RD-000001";
	
	private final RoundQueryRepository roundQueryRepository;
	
	@Override
	public String nextCode() {
		Optional<String> optionalLatestRoundCode = roundQueryRepository.getLatestRoundCode();
		
		if (optionalLatestRoundCode.isPresent()) {
			String latestRoundCode = optionalLatestRoundCode.get();
			return getIncrementedCode(latestRoundCode, CODE_PREFIX, CODE_PATTERN);
		}
		return INIT_CODE;
	}

}
