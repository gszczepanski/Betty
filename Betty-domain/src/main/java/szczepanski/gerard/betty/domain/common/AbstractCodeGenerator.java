package szczepanski.gerard.betty.domain.common;

import java.util.List;

import szczepanski.gerard.betty.common.exception.BettyRuntimeException;
import szczepanski.gerard.betty.common.util.BettyStringUtils;

public abstract class AbstractCodeGenerator {
	
	protected String nextCode() {
		throw new BettyRuntimeException("This method must be implemented in derived class!");
	}
	
	protected List<String> nextCodes(int nextCodesNumber) {
		throw new BettyRuntimeException("This method must be implemented in derived class!");
	}
	
	protected final String getIncrementedCode(String latestRoundCode, String codePrefix, String codePattern) {
		String codeNumber = latestRoundCode.replace(codePrefix, BettyStringUtils.EMPTY);
		Integer codeNumberAsInteger = Integer.valueOf(codeNumber);
		
		Integer incrementedNumber = codeNumberAsInteger + 1;
		return codePrefix + String.format(codePattern, incrementedNumber);
	}
	
}
