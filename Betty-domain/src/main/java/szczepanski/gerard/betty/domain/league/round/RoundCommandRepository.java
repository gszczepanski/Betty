package szczepanski.gerard.betty.domain.league.round;

import szczepanski.gerard.betty.domain.league.round.model.Round;

public interface RoundCommandRepository {
	
	void create(Round newRound);
	
}
