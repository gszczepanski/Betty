/**
 * 
 */
package szczepanski.gerard.betty.domain.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories({ "szczepanski.gerard.betty.domain" })
public class PersistenceJpaConfiguration {

	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(PersistenceConfig.DRIVER_CLASS_NAME);
		dataSource.setUrl(PersistenceConfig.DB_URL);
		dataSource.setUsername(PersistenceConfig.DB_USER);
		dataSource.setPassword(PersistenceConfig.DB_PASS);
		return dataSource;
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean entityManager = new LocalContainerEntityManagerFactoryBean();
		entityManager.setDataSource(dataSource());
		entityManager.setPackagesToScan(PersistenceConfig.PACKAGES_TO_SCAN_FOR_ENTITIES);
		entityManager.setJpaProperties(getJpaProperties());

		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		entityManager.setJpaVendorAdapter(vendorAdapter);

		return entityManager;
	}

	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(emf);
		return transactionManager;
	}

	private Properties getJpaProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.show_sql", PersistenceConfig.HIBERNATE_SHOW_SQL);
		properties.setProperty("hibernate.dialect", PersistenceConfig.HIBERNATE_DIALECT);
		properties.setProperty("hibernate.hbm2ddl.auto", PersistenceConfig.HIBERNATE_SCHEMA_GENERATE);
		return properties;
	}

}