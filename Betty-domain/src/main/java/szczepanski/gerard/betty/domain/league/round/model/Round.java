package szczepanski.gerard.betty.domain.league.round.model;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import szczepanski.gerard.betty.domain.common.Schema;
import szczepanski.gerard.betty.domain.common.model.AbstractEntity;
import szczepanski.gerard.betty.domain.league.season.model.Season;

@Entity
@Table(schema = Schema.BETTING, name = "ROUND")
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Round extends AbstractEntity {
	
	@Column(name = "CODE")
	private String code;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SEASON_ID")
	private Season season;
	
	@Enumerated(EnumType.STRING)
	@JoinColumn(name = "STATE")
	private RoundState state;
	
	@OneToMany(mappedBy = "round")
	private Set<RoundFixture> roundFixtures;
	
	@Column(name = "START_DATE")
	private LocalDateTime startDate;
	
	@Column(name = "FINISH_DATE")
	private LocalDateTime finishDate;
	
	public Round(Long id) {
		setId(id);
	}
	
}
