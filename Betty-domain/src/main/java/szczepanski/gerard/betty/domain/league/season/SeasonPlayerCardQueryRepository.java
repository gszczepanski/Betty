package szczepanski.gerard.betty.domain.league.season;

import szczepanski.gerard.betty.domain.league.season.model.SeasonPlayerCard;

public interface SeasonPlayerCardQueryRepository {
	
	SeasonPlayerCard getPlayerCardByBettyUserId(Long bettyUserId);
	
}
