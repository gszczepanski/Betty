package szczepanski.gerard.betty.domain.league.season.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import szczepanski.gerard.betty.domain.common.Schema;
import szczepanski.gerard.betty.domain.common.model.AbstractEntity;
import szczepanski.gerard.betty.domain.league.model.League;
import szczepanski.gerard.betty.domain.league.round.model.Round;

@Getter
@Builder
@AllArgsConstructor
@Entity
@Table(schema = Schema.BETTING, name = "SEASON")
public class Season extends AbstractEntity {
	
	@Column(name = "CODE")
	private String code;
	
	@ManyToOne
	@JoinColumn(name = "LEAGUE_ID")
	private League league;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "STATE")
	private SeasonState state;
	
	@Column(name = "CURRENT_ROUND_NUMBER")
	private Integer currentRoundNumber;
	
	@Column(name = "TOTAL_ROUNDS_NUMBER")
	private Integer totalRoundsNumber;
	
	@OneToMany(mappedBy = "season")
	private Set<Round> rounds;
	
	public Season(Long id) {
		setId(id);
	}
}
