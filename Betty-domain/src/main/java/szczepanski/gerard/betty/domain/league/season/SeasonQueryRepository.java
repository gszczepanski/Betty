package szczepanski.gerard.betty.domain.league.season;

public interface SeasonQueryRepository {
	
	Boolean canAddNewRound(Long seasonId);
	
}
