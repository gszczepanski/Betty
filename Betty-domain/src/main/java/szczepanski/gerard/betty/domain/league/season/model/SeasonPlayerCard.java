package szczepanski.gerard.betty.domain.league.season.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.NoArgsConstructor;
import szczepanski.gerard.betty.domain.common.Schema;
import szczepanski.gerard.betty.domain.common.model.AbstractEntity;
import szczepanski.gerard.betty.domain.league.bet.model.RoundBet;
import szczepanski.gerard.betty.domain.user.model.BettyUser;

@Entity
@Table(schema = Schema.BETTING, name = "SEASON_PLAYER_CARD")
@NoArgsConstructor
public class SeasonPlayerCard extends AbstractEntity {
	
	@Column(name = "CODE")
	private String code;
	
	@ManyToOne
	@JoinColumn(name = "SEASON_ID")
	private Season season;
	
	@ManyToOne
	@JoinColumn(name = "BETTY_USER_ID")
	private BettyUser player;
	
	@OneToMany(mappedBy = "playerCard")
	private Set<RoundBet> roundBets;
	
	@Column(name = "TOTAL_POINTS")
	private Integer totalPoints;
	
	public SeasonPlayerCard(Long id) {
		setId(id);
	}
}
