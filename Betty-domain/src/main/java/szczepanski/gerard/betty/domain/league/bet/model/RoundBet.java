package szczepanski.gerard.betty.domain.league.bet.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Builder;
import lombok.Getter;
import szczepanski.gerard.betty.domain.common.Schema;
import szczepanski.gerard.betty.domain.common.model.AbstractEntity;
import szczepanski.gerard.betty.domain.league.round.model.Round;
import szczepanski.gerard.betty.domain.league.season.model.SeasonPlayerCard;

@Entity
@Table(schema = Schema.BETTING, name = "ROUND_BET")
@Builder
@Getter
public class RoundBet extends AbstractEntity {
	
	@Column(name = "CODE")
	private String code;
	
	@OneToMany(mappedBy = "roundBet")
	private Set<Bet> bets;
	
	@ManyToOne
	@JoinColumn(name = "ROUND_ID")
	private Round forRound;
	
	@ManyToOne
	@JoinColumn(name = "PLAYER_CARD_ID")
	private SeasonPlayerCard playerCard;
	
	@Column(name = "POINTS_GAINED")
	private Integer pointsGained;

}
