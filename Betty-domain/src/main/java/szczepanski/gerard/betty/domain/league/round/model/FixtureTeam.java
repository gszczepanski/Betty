package szczepanski.gerard.betty.domain.league.round.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import szczepanski.gerard.betty.domain.common.Schema;
import szczepanski.gerard.betty.domain.common.model.AbstractEntity;

@Entity
@Table(schema = Schema.BETTING, name = "FIXTURE_TEAM")
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FixtureTeam extends AbstractEntity {
	
	@Column(name = "SYNCHRONIZATION_TEAM_ID")
	private String synchronizationTeamId;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "IMAGE_PATH")
	private String imagePath;
	
}
