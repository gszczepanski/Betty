package szczepanski.gerard.betty.domain.league.season.model;

public enum SeasonState {
	
	IN_PROGRESS,
	FINISHED;
	
}
