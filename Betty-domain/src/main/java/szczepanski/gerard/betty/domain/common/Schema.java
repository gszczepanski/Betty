package szczepanski.gerard.betty.domain.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Schema {	
	
	public static final String USER = "user";
	public static final String BETTING = "betting";

}
