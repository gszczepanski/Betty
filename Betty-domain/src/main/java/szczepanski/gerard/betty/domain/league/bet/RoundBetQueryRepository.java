package szczepanski.gerard.betty.domain.league.bet;

import java.util.Optional;

public interface RoundBetQueryRepository {
	
	Optional<String> getLatestRoundBetCode();
	
	Optional<String> getLatestBetCode();
	
}
