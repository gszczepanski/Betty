/**
 * 
 */
package szczepanski.gerard.betty.domain.config;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Temporary local db configuration for development
 *
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PersistenceConfig {
	
	static final String DRIVER_CLASS_NAME = "org.postgresql.Driver";
	static final String DB_USER = "postgres";
	static final String DB_PASS = "toor";
	static final String DB_URL = "jdbc:postgresql://localhost:5433/betty";
	static final String HIBERNATE_DIALECT = "org.hibernate.dialect.PostgreSQLDialect";
	static final String HIBERNATE_SHOW_SQL = "true";
	static final String HIBERNATE_SCHEMA_GENERATE = "update";
	
	static final String[] PACKAGES_TO_SCAN_FOR_ENTITIES = {
				"szczepanski.gerard.betty.domain.league.model",
				"szczepanski.gerard.betty.domain.league.bet.model",
				"szczepanski.gerard.betty.domain.league.round.model",
				"szczepanski.gerard.betty.domain.league.season.model",
				"szczepanski.gerard.betty.domain.user.model"
			};
	
}
