package szczepanski.gerard.betty.domain.league.round;

import java.util.Optional;

public interface RoundQueryRepository {
	
	Boolean isAllRoundsAreFinishedForSeason(Long seasonId);
	
	Optional<String> getLatestRoundCode();
	
	Optional<String> getLatestRoundFixtureCode();
	
}
