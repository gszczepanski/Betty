package szczepanski.gerard.betty.domain.league.round.model;

public enum RoundFixtureType {
	
	NORMAL,
	BONUS;
	
	public static Boolean canBeMappedFromString(String type) {
		try {
			RoundFixtureType.valueOf(type);
			return true;
		} catch (Throwable e) {
			return false;
		}
	}
	
}
