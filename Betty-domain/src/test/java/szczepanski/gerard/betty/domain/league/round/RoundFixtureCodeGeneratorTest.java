package szczepanski.gerard.betty.domain.league.round;

import java.util.List;
import java.util.Optional;

import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

@Test(groups = "unit-tests")
public class RoundFixtureCodeGeneratorTest {

	RoundQueryRepository roundQueryRepository;
	RoundFixtureCodeGenerator roundFixtureCodeGenerator;

	@BeforeMethod
	public void beforeMethod() {
		roundQueryRepository = Mockito.mock(RoundQueryRepository.class);

		roundFixtureCodeGenerator = RoundFixtureCodeGenerator.builder().roundQueryRepository(roundQueryRepository).build();
	}

	@Test
	public void whenLatestRoundFixtureCodeNotFoundInDbThenGenerateFirstCode() {
		// Arrange
		String firstExpectedCode = "FIX-000001";
		String secondExpectedCode = "FIX-000002";

		Integer numberOfCodesToGenerate = 2;
		Mockito.when(roundQueryRepository.getLatestRoundFixtureCode()).thenReturn(Optional.empty());

		// Act
		List<String> generatedCodes = roundFixtureCodeGenerator.nextCodes(numberOfCodesToGenerate);

		// Assert
		Assert.assertNotNull(generatedCodes);
		Assert.assertFalse(generatedCodes.isEmpty());
		Assert.assertEquals(generatedCodes.get(0), firstExpectedCode);
		Assert.assertEquals(generatedCodes.get(1), secondExpectedCode);

		Mockito.verify(roundQueryRepository).getLatestRoundFixtureCode();
	}

	@Test
	public void whenLatestRoundFixtureCodeFoundInDbThenGenerateNextCodeBasedOnFoundCode() {
		// Arrange
		Optional<String> foundLatestCode = Optional.of("FIX-000004");
		
		String firstExpectedCode = "FIX-000005";
		String secondExpectedCode = "FIX-000006";

		Integer numberOfCodesToGenerate = 2;
		Mockito.when(roundQueryRepository.getLatestRoundFixtureCode()).thenReturn(foundLatestCode);

		// Act
		List<String> generatedCodes = roundFixtureCodeGenerator.nextCodes(numberOfCodesToGenerate);

		// Assert
		Assert.assertNotNull(generatedCodes);
		Assert.assertFalse(generatedCodes.isEmpty());
		Assert.assertEquals(generatedCodes.get(0), firstExpectedCode);
		Assert.assertEquals(generatedCodes.get(1), secondExpectedCode);

		Mockito.verify(roundQueryRepository).getLatestRoundFixtureCode();
	}
}
