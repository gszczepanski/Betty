package szczepanski.gerard.betty.domain.league.bet;

import java.util.Optional;

import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class RoundBetCodeGeneratorTest {
	
	RoundBetQueryRepository roundBetQueryRepository;
	RoundBetCodeGenerator roundBetCodeGenerator;
	
	@BeforeMethod
	public void beforeMethod() {
		roundBetQueryRepository = Mockito.mock(RoundBetQueryRepository.class);
		
		roundBetCodeGenerator = RoundBetCodeGenerator.builder()
				.roundBetQueryRepository(roundBetQueryRepository)
				.build();
	}
	
	@Test
	public void whenLatestRoundCodeNotFoundInDbThenGenerateFirstCode() {
		// Arrange
		String expectedFirstCode = "RBET-000001";
		
		Mockito.when(roundBetQueryRepository.getLatestRoundBetCode()).thenReturn(Optional.empty());
		
		// Act
		String generatedCode = roundBetCodeGenerator.nextCode();
		
		// Assert
		Assert.assertNotNull(generatedCode);
		Assert.assertEquals(generatedCode, expectedFirstCode);
		
		Mockito.verify(roundBetQueryRepository).getLatestRoundBetCode();
	}
	
	
	@Test
	public void whenLatestRoundCodeFoundInDbThenGenerateNextCodeBasedOnFoundCode() {
		// Arrange
		Optional<String> latestFoundCode = Optional.of("RBET-000004");
		String expectedGeneratedCode = "RBET-000005";
		
		Mockito.when(roundBetQueryRepository.getLatestRoundBetCode()).thenReturn(latestFoundCode);
		
		// Act
		String generatedCode = roundBetCodeGenerator.nextCode();
		
		// Assert
		Assert.assertNotNull(generatedCode);
		Assert.assertEquals(generatedCode, expectedGeneratedCode);
		
		Mockito.verify(roundBetQueryRepository).getLatestRoundBetCode();
	}
}
