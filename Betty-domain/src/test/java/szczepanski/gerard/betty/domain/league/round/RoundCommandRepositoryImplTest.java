package szczepanski.gerard.betty.domain.league.round;

import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import szczepanski.gerard.betty.common.assertion.ParamAssertionException;
import szczepanski.gerard.betty.domain.league.round.model.Round;

@Test(groups = "unit-tests")
public class RoundCommandRepositoryImplTest {
	
	RoundDao roundDao;
	RoundCommandRepository roundCommandRepository;
	
	@BeforeMethod
	public void beforeMethod() {
		roundDao = Mockito.mock(RoundDao.class);
		roundCommandRepository = RoundCommandRepositoryImpl.builder()
				.roundDao(roundDao)
				.build();
	}
	
	@Test
	public void createRoundSuccess() {
		// Arrange
		Round newRound = new Round();
		
		// Act
		roundCommandRepository.create(newRound);
		
		// Assert
		Mockito.verify(roundDao).save(newRound);
	}
	
	@Test(expectedExceptions = ParamAssertionException.class)
	public void whenRoundIsNullWhileCreateRoundProcessThenThrowParamAssertion() {
		// Arrange
		Round NULL_ROUND = null;
				
		// Act
		roundCommandRepository.create(NULL_ROUND);
	}

}
