package szczepanski.gerard.betty.domain.league.bet;

import java.util.List;
import java.util.Optional;

import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class BetCodeGeneratorTest {

	RoundBetQueryRepository roundBetQueryRepository;
	BetCodeGenerator betCodeGenerator;

	@BeforeMethod
	public void beforeMethod() {
		roundBetQueryRepository = Mockito.mock(RoundBetQueryRepository.class);

		betCodeGenerator = BetCodeGenerator.builder()
				.roundBetQueryRepository(roundBetQueryRepository)
				.build();
	}

	@Test
	public void whenLatestRoundFixtureCodeNotFoundInDbThenGenerateFirstCode() {
		// Arrange
		String firstExpectedCode = "BET-000001";
		String secondExpectedCode = "BET-000002";

		Integer numberOfCodesToGenerate = 2;
		Mockito.when(roundBetQueryRepository.getLatestBetCode()).thenReturn(Optional.empty());

		// Act
		List<String> generatedCodes = betCodeGenerator.nextCodes(numberOfCodesToGenerate);

		// Assert
		Assert.assertNotNull(generatedCodes);
		Assert.assertFalse(generatedCodes.isEmpty());
		Assert.assertEquals(generatedCodes.get(0), firstExpectedCode);
		Assert.assertEquals(generatedCodes.get(1), secondExpectedCode);

		Mockito.verify(roundBetQueryRepository).getLatestBetCode();
	}

	@Test
	public void whenLatestRoundFixtureCodeFoundInDbThenGenerateNextCodeBasedOnFoundCode() {
		// Arrange
		Optional<String> foundLatestCode = Optional.of("BET-000004");
		
		String firstExpectedCode = "BET-000005";
		String secondExpectedCode = "BET-000006";

		Integer numberOfCodesToGenerate = 2;
		Mockito.when(roundBetQueryRepository.getLatestBetCode()).thenReturn(foundLatestCode);

		// Act
		List<String> generatedCodes = betCodeGenerator.nextCodes(numberOfCodesToGenerate);

		// Assert
		Assert.assertNotNull(generatedCodes);
		Assert.assertFalse(generatedCodes.isEmpty());
		Assert.assertEquals(generatedCodes.get(0), firstExpectedCode);
		Assert.assertEquals(generatedCodes.get(1), secondExpectedCode);

		Mockito.verify(roundBetQueryRepository).getLatestBetCode();
	}
}
