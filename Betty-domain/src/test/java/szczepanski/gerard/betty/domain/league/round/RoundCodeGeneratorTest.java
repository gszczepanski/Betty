package szczepanski.gerard.betty.domain.league.round;

import java.util.Optional;

import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

@Test(groups = "unit-tests")
public class RoundCodeGeneratorTest {
	
	RoundQueryRepository roundQueryRepository;
	RoundCodeGenerator roundCodeGenerator;
	
	@BeforeMethod
	public void beforeMethod() {
		roundQueryRepository = Mockito.mock(RoundQueryRepository.class);
		
		roundCodeGenerator = RoundCodeGenerator.builder()
				.roundQueryRepository(roundQueryRepository)
				.build();
	}
	
	@Test
	public void whenLatestRoundCodeNotFoundInDbThenGenerateFirstCode() {
		// Arrange
		String expectedFirstCode = "RD-000001";
		
		Mockito.when(roundQueryRepository.getLatestRoundCode()).thenReturn(Optional.empty());
		
		// Act
		String generatedCode = roundCodeGenerator.nextCode();
		
		// Assert
		Assert.assertNotNull(generatedCode);
		Assert.assertEquals(generatedCode, expectedFirstCode);
		
		Mockito.verify(roundQueryRepository).getLatestRoundCode();
	}
	
	
	@Test
	public void whenLatestRoundCodeFoundInDbThenGenerateNextCodeBasedOnFoundCode() {
		// Arrange
		Optional<String> latestFoundCode = Optional.of("RD-000004");
		String expectedGeneratedCode = "RD-000005";
		
		Mockito.when(roundQueryRepository.getLatestRoundCode()).thenReturn(latestFoundCode);
		
		// Act
		String generatedCode = roundCodeGenerator.nextCode();
		
		// Assert
		Assert.assertNotNull(generatedCode);
		Assert.assertEquals(generatedCode, expectedGeneratedCode);
		
		Mockito.verify(roundQueryRepository).getLatestRoundCode();
	}
}
