package szczepanski.gerard.betty.common.assertion;

import java.util.Collection;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ParamAssertion {

	public static void isNotNull(Object... object) {
		for (Object o : object) {
			if (o == null) {
				throw new ParamAssertionException("Assertion failed! Object is null!");
			}
		}
	}

	public static void collectionNotEmpty(Collection<?> collection) {
		if (collection == null) {
			throw new ParamAssertionException("Assertion failed! Collection is null!");
		}

		if (collection.isEmpty()) {
			throw new ParamAssertionException("Assertion failed! Collection is empty");
		}
	}

	public static void collectionContainsAtLeastElements(Collection<?> collection, int minimumNumberOfElements) {
		if (collection == null) {
			throw new ParamAssertionException("Assertion failed! Collection is null!");
		}

		if (collection.size() < minimumNumberOfElements) {
			throw new ParamAssertionException(
					String.format("Assertion failed! Collection has'nt required minimum %d number of elements", minimumNumberOfElements));
		}
	}

	public static void checkIfTheSame(Object first, Object second) {
		if (!first.equals(second)) {
			throw new ParamAssertionException("Assertion failed! Objects are not the same");
		}
	}

	public static void isNumberRepresentation(String value) {
		try {
			Double.parseDouble(value);
		} catch (NumberFormatException nfe) {
			throw new ParamAssertionException(String.format("Given String value: %s is not numeric!", value));
		}
	}

	public static void isNotBlank(String text) {
		if (text == null || "".equals(text)) {
			throw new ParamAssertionException("String is blank!");
		}
	}

	public static void isNotBlank(String... texts) {
		for (int i = 0; i < texts.length; i++) {
			isNotBlank(texts[i]);
		}
	}
	
	public static void isValidStringNumericId(String id) {
		isNotBlank(id);
		isNumberRepresentation(id);
		
		Integer numberId = Integer.valueOf(id);
		if (numberId < 0) {
			throw new ParamAssertionException(String.format("Given String numeric id: %s is smaller than 0!", id));
		}
	}
	
	public static void isValidLongId(Long id) {
		isNotNull(id);
		
		if (id < 0) {
			throw new ParamAssertionException(String.format("Given Long id: %s is smaller than 0!", id));
		}
	}
}
