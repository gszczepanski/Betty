package szczepanski.gerard.betty.common.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BettyStringUtils {
	
	public static final String EMPTY = "";
	
}
