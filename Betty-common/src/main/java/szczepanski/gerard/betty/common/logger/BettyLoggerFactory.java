package szczepanski.gerard.betty.common.logger;

import org.apache.log4j.Logger;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BettyLoggerFactory {
	
	public static BettyLogger createForName(String name) {
		return new BettyLoggerFactoryImpl(name);
	}
	
	private static class BettyLoggerFactoryImpl implements BettyLogger {
		
		private final Logger log;
		
		private BettyLoggerFactoryImpl(String name) {
			this.log = Logger.getLogger(name);
		}

		@Override
		public void info(String msg) {
			info(msg, new Object[]{});
		}

		@Override
		public void info(String msg, Object... args) {
			log.info(String.format(msg, args));
		}

		@Override
		public void debug(String msg) {
			debug(msg, new Object[]{});
		}

		@Override
		public void debug(String msg, Object... args) {
			log.info(String.format(msg, args)); //TMP change from debug to info to see logs on server
		}
		
		@Override
		public void error(String msg, Throwable error) {
			error(msg, error, new Object[]{});
		}

		@Override
		public void error(String msg, Throwable error, Object... args) {
			log.error(String.format(msg, args), error);
		}
		
	}
	
}
