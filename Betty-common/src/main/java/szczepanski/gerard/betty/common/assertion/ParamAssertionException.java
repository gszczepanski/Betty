package szczepanski.gerard.betty.common.assertion;

import szczepanski.gerard.betty.common.exception.BettyRuntimeException;

public class ParamAssertionException extends BettyRuntimeException {
	private static final long serialVersionUID = -8709772465181201706L;

	public ParamAssertionException() {
		super();
	}

	public ParamAssertionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ParamAssertionException(String message, Throwable cause) {
		super(message, cause);
	}

	public ParamAssertionException(String message) {
		super(message);
	}

	public ParamAssertionException(Throwable cause) {
		super(cause);
	}

}
