package szczepanski.gerard.betty.common.exception;

public class BettyException extends Exception {
	private static final long serialVersionUID = 833734168633475907L;

	public BettyException() {
		super();
	}

	public BettyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public BettyException(String message, Throwable cause) {
		super(message, cause);
	}

	public BettyException(String message) {
		super(message);
	}

	public BettyException(Throwable cause) {
		super(cause);
	}
	
}
