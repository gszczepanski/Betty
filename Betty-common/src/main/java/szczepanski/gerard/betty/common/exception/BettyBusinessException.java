package szczepanski.gerard.betty.common.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class BettyBusinessException extends BettyException {
	private static final long serialVersionUID = 1637550252142918846L;
	
	@Getter
	private final String msgCode;
	
}
