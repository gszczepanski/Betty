package szczepanski.gerard.betty.common.exception;

public class BettyRuntimeException extends RuntimeException {
	private static final long serialVersionUID = -3893772539915492509L;

	public BettyRuntimeException() {
		super();
	}

	public BettyRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public BettyRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public BettyRuntimeException(String message) {
		super(message);
	}

	public BettyRuntimeException(Throwable cause) {
		super(cause);
	}
	
}
